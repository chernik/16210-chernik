#include <iostream>

template <class T> class SharedPtr {
public:
	SharedPtr(T* el){
		this->el = el;
		std::cout << "NEW\n";
		this->count = new int[1];
		this->count[0] = 1;
	}
	
	SharedPtr(const SharedPtr& other){
		this->el = other.el;
		this->count = other.count;
		this->inc();
	}
	
	SharedPtr& operator=(const SharedPtr& other){
		this->dec();
		this->count = other.count;
		this->el = other.el;
		this->inc();
		return *this;
	}
	
	T* operator->(){
		return this->el;
	}
	
	T& operator*(){
		return this->el[0];
	}	
	
	virtual ~SharedPtr(){
		this->dec();
	}
private:
	T* el;
	int* count;
	
	void inc(){
		std::cout << "INC\n";
		this->count[0]++;
	}
	
	void dec(){
		std::cout << "DEC\n";
		this->count[0]--;
		if(this->count[0] == 0){
			std::cout << "DELETE\n";
			delete this->el;
			delete this->count;
		}
	}
};

class TestClass {
public:
	TestClass(int a){
		this->data = a;
	}
	
	void foo(){
		std::cout << "TestClass " << this->data << "\n";
	}
	
	virtual ~TestClass(){};
private:
	int data;
};

int main(){
	SharedPtr<TestClass> a(new TestClass(3));
	SharedPtr<TestClass> b(new TestClass(5));
	SharedPtr<TestClass> c = a;
	a->foo();
	b->foo();
	c->foo();
}
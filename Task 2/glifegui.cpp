#include "glifegui.h"

namespace Chernik {
	namespace GLifeF {
		GLdriver drv;
		MatrixCube cubes;
		Options GUIopt;
		bool fl = false;
		
		void draw(){
			drv.startDraw();
			cubes.out();
			drv.endDraw();
		}
		
		void move(int x, int y){
			drv.setCamPos()
				(- 1.0 * x / 300 * 12 + 6)
				(+ 1.0 * y / 300 * 12 - 6);
			draw();
		}
		
		void roll(int but, int state, int, int){
			if(state != 0)
				return;
			if(but == 0)
				drv.setCamPos()----(drv.setCamPos()----++ + 0.5);
			else
				drv.setCamPos()----(drv.setCamPos()----++ - 0.5);
			draw();
		}
		
		void console(int a){
			if(!fl) return;
			fl = false;
			GLifeF::GUIopt["console"]();
			glutShowWindow();
		}
		
		void key(unsigned char a, int, int){
			if(a == 27){
				glutTimerFunc(10, console, 0);
				glutHideWindow();
				fl = true;
			}
			if(a == 13){
				GLifeF::GUIopt["stepone"]();
				GLifeF::GUIopt["assign"]();
				draw();
			}
		}
		
		/*void vis(int state){
			std::cout << "visible " << state << "\n";
			std::string a;
			switch(state){
			case 0:
				break;
			case 1:
				break;
			default:
				std::runtime_error(
					std::string("Unknown window state > ") + std::string(Options::Option::OptionData(state)));
			}
			glutVisibilityFunc(0);
		}*/
		
		void init(Options& opt){
			drv.setEventDraw(draw);
			drv.setEventMoveM(move);
			drv.setEventButM(roll);
			drv.setEventKey(key);
			//drv.setEventVisible(vis);
			GUIopt = opt;
		}
	}
}
#include "glifedriver.h"

namespace Chernik {
// --------------------------------------------------------------------------------------------------- standart constructor
	GLifeDriver::GLifeDriver(){}
// ---------------------------------------------------------------------------------------------------------------- copying
	GLifeDriver::GLifeDriver(const GLifeDriver& other){
		this->argv = other.argv;
	}
	
	GLifeDriver&
	GLifeDriver::operator=(const GLifeDriver& other){
		this->argv = other.argv;
		return *this;
	}
// -------------------------------------------------------------------------------------------------------- public methods
	void GLifeDriver::argCV(int argc, char** argv){
		this->argv = Options::Parser::Helper::
			argCVparse(argc, argv);
	}
	
	void GLifeDriver::go(){
		if(this->argv.size() > 0){ // ���� ��㬥���
			this->goArgCV(); // ��⮬���᪨� ०��
			return;
		}
		this->goConsole(); // ���ࠪ⨢�� ०��
	}
// ------------------------------------------------------------------------------------------------------- private methods
	void GLifeDriver::addHelp(pOptions opt, std::string name, const Options::Option::VecStr& flags, std::string desc){
		opt->addOption(Options::Option(
			[opt](int, Options::Option::OptionData& data){
				std::string sData = data; // ��६ ��ࠬ���
				if(sData == "all" || sData == "nil"){ // �᫨ ���� �뢮���� �� ���� ��ࠬ��� �� 㪠���
					data = ""; // ����뢠��, �� ���� �뢥�� ��
					return std::string(""); // �� ��ଠ�쭮
				}
				std::string name = opt->getNameFromFlag(sData); // ��६ ��� ��樨 �� 㪠������� 䫠��
				if(name.size() == 0) // �᫨ �� ��諨
					return std::string("No find <" + sData + ">. Try 'all'"); // �訡��
				data = name; // ��� � ����⢥ ��ࠬ���
				return std::string(""); // �� ��ଠ�쭮
			},
			name, // ��������� ���
			flags, // �������� 䫠��
			"<option>\nHelp out for " + desc + ". Type option or 'all' for out all options", // ���ᠭ��
			1, // ���� ��ࠬ���
			[opt](Options::Option& myOpt){
				std::string name = myOpt.getData(0); // ��६ ���
				if(name.size() == 0) // �᫨ �뢮��� ��
					std::cout << opt->getConsole(); // �뢮��� ��
				else // ����
					std::cout << (*opt)[name].console(); // �� �뢮��� ��
				return ""; // �� ��ଠ�쭮
			}
		));
	}
	
	std::string GLifeDriver::saveFile(std::string name){
		Options::Parser::Helper::VecStr arr, buf; // ���ᨢ� ��� 䠩��
		auto cast = [](int data){ // int => std::string
			return std::string(Options::Option::OptionData(data));
		};
		auto rules = this->obj.getData(); // ��६ �����
		int cnt; // ���稪
		// �����ࠥ��� � ��䮫��묨 �㭪�ﬨ
		auto goDef = [cast, &rules, &cnt, &arr, &buf]
		(const std::vector<std::string>& names, std::string name){
			buf.clear();
			cnt = 0;
			buf.push_back(name);
			for(auto&& iter : names){ // ��⠥�, ᪮�쪮 ������ �� ��䮫��
				if(!rules[iter].isDef())
					cnt++;
				buf.push_back(cast(rules[iter].get()));
			}
			if(cnt > 0) // �᫨ ��� �� ���� ��ࠬ���
				arr.push_back(buf.join(" "));
		};
		goDef({"M", "N", "K"}, "RULES");
		goDef({"X", "Y"}, "FIELD");
		auto matrix = this->obj.getMatrix();
		int i, j;
		for(i = 0; i < matrix.getSizeY(); i++)
		for(j = 0; j < matrix.getSizeX(); j++){
			if(matrix.get(j, i) > 0){
				buf.clear();
				buf.push_back("SET");
				buf.push_back(cast(i));
				buf.push_back(cast(j));
				arr.push_back(buf.join(" "));
			}
		}
		std::fstream f; // ����
		f.open(name, std::fstream::out); // ���뢠��
		if(!f.is_open()) // �᫨ �� ���뫨
			return "File don't open"; // �訡��
		f << std::string(arr.join("\n"));
		f.close();
		return "";
	}
	
	std::string GLifeDriver::loadFile(std::string name){
		this->obj.doubleState();
		// ------------------------------------------------------ ���뢠�� 䠩�
		std::fstream f; // ����
		Options::Parser::Helper::String strInput; // ��ப� ��� �����
		Options::Parser::Helper::VecStr arrInput, arrAll; // ���ᨢ ��ப
		f.open(name, std::fstream::in); // ���뢠��
		if(!f.is_open()) // �᫨ �� ���뫨
			return "File don't open"; // �訡��
		// --------------------------------------------------------- ����㦠�� ��ப�
		while(!f.eof()){ // ���� 䠩�...
			getline(f, strInput); // ...���뢠�� �� ��ப��...
			arrInput.push_back(strInput); // ...� �����뢠�� � ���ᨢ.
		}
		// ----------------------------------------------------------- ����뢠�� 䠩�
		f.close(); // ����뢠��
		// -------------------------------------------------------------- ���ࠥ� �������ਨ � ᮥ���塞 ��ப�
		for(auto&& iter : arrInput){
			iter = iter.split("#")[0];
			auto strBuf = iter;
			auto strParse = strBuf.split(" ");
			arrAll.insert(arrAll.end(), strParse.begin(), strParse.end());
		}
		// ---------------------------------------------- ���ᨬ field & rules, ������㥬 set.
		pOptions opt = this->getOptFile();
		(*opt)["set"].setIgnore(true);
		Options::Parser parse(*opt);
		parse.parse(arrAll);
		if(parse.isError())
			return parse.getErrors()[0]->console();
		this->obj.setBack(false);	
		parse.getOptions().run();
		this->obj.setBack(true);
		// ---------------------------------------------- ���ᨬ ⮫쪮 set
		(*opt)["set"].setIgnore(false);
		(*opt)["rules"].setIgnore(true);
		(*opt)["field"].setIgnore(true);
		parse = Options::Parser(*opt);
		size_t i;
		for(i = 0; i < arrInput.size(); i++){
			auto iter = arrInput[i];
			parse.parse(iter.split(" "));
			if(parse.isError())
				return std::string("Line ") +
					std::string(Options::Option::OptionData(int(i+1))) + ":\n" +
					parse.getErrors()[0]->console();
			this->obj.setBack(false);
			parse.getOptions().run();
			this->obj.setBack(true);
		}
		return "";
	}
// -------------------------------------------------------------------------------------------------------------- runners
	void GLifeDriver::goArgCV(){
		this->obj.doubleState();
		pOptions opt = this->getOptArgCV(); // �����㦠�� ������� ��� ��㬥�⮢
		Options::Parser p(*opt); // �����
		p.parse(this->argv); // ���ᨬ
		if(p.isError()){ // �᫨ �訡�� ���ᨭ��
			std::cout <<
				p.getErrors()[0]->console()
				<< "\n\nPush --help of -h for more information\n";
			return; // ��室
		}
		Options& resOpt = p.getOptions(); // ��६ १������騥 ��樨
		if(resOpt["help"].isFull()){ // ���� �⤥�쭮
			resOpt["help"].run();
			return; // ��⠫쭮� ������㥬
		}
		auto err = [&resOpt](){ // �뢮� ⥪�� �訡��
			std::cout << "You should use this commands for correct work:\n\n";
			std::cout << resOpt["loadf"].console();
			std::cout << resOpt["step"].console();
			std::cout << "\nOr use help:\n\n";
			std::cout << resOpt["help"].console();
		};
		auto run = [this, &resOpt, err](const std::vector<std::pair<std::string, bool>>& arr){ // �믮����� ��権
			for(auto&& iter : arr){
				if(resOpt[iter.first].isFull()){
					this->obj.setBack(false);
					std::string res = resOpt[iter.first].run();
					this->obj.setBack(true);
					if(res.size() > 0){
						std::cout << "Error > " << res << "\n";
						return false;
					}
				} else {
					if(iter.second){
						err();
						return false;
					}
				}
			}
			return true;
		};
		if(!run({ // �믮��塞
			{"loadf", true},
			{"rulesM", false},
			{"rulesN", false},
			{"rulesK", false},
			{"field", false},
			{"step", true}
		})) return; // �᫨ ��-� �� ⠪, ���뢠����
		if(resOpt["savef"].isFull()){ // �᫨ ���� ��࠭��� � 䠩�
			std::string res = resOpt["savef"].run();
			if(res.size() > 0){ // �᫨ �訡��
				std::cout << "Error > " << res << "\n";
				return;
			}
		} else { // �᫨ �� ���� ��࠭���
			resOpt["out"](); // ���� �뢮���
		}
	}
	
	void GLifeDriver::goConsole(){
		pOptions opt = this->getOptConsole(); // �����㦠�� ������� ��� ���᮫�
		bool fl = true; // ���� 横��
		opt->addOption(Options::Option( // ���� ��� ��室�
			"exit",
			{"exit"},
			"Exit from the console",
			0,
			[&fl](Options::Option&){fl = false; return "";}
		));
		//(*opt)["gui"]();
		Options::Parser::Helper::String cons; // ��ப� ��� �����
		Options::Parser parse(*opt); // �����
		while(fl){ // ���� ���᮫�
			std::cout << " > "; // ���� ��������
			getline(std::cin, cons);
			cons = cons.split("#")[0]; // ���ࠥ� �������਩ (� ���� ��?)
			cons += " nil"; // ����������� ����/��㬥�� ��� step ��� ��ࠬ��஢ (����뫨���������)
			parse.parse(cons.split(" ")); // ����� �� �஡����
			if(parse.isError()) // �᫨ �訡��, �뢮���
				std::cout << parse.getErrors()[0]->console();
			auto res = parse.getOptions().run(); // �믮��塞
			if(res.size() > 0)
				std::cout << "Error > " << res << "\n";
			parse.getOptions()["out"](); // �뢮��� ⠡���� �� ��࠭
		}
	}
	
	void GLifeDriver::goGUI(){
		pOptions opt = this->getOptGUI();
		GLifeF::init(*opt);
		GLifeF::GUIopt["assign"]();
		GLifeF::drv(); // ��᪮���� 横� ��� ��室�
	}
// ------------------------------------------------------------------------------------------------------------- option getters
	GLifeDriver::pOptions GLifeDriver::getOptArgCV(){
		pOptions optReal = this->getOptConsole(); // �����㦠�� �ࠢ����� �� ���᮫�
		pOptions opt = newOptions();
		// ��६ �㦭� ��樨
		opt->addOption(Options::Option({"--input", "-if"}, (*optReal)["loadf"]));
		opt->addOption(Options::Option({"--output", "-o"}, (*optReal)["savef"]));
		opt->addOption(Options::Option({"--iterations", "-ic"}, (*optReal)["step"]));
		opt->addOption(Options::Option({"--field", "-f"}, (*optReal)["field"]));
		opt->addOption(Options::Option({}, (*optReal)["rules"]));
		opt->addOption(Options::Option({}, (*optReal)["out"]));
		this->addHelp(opt, "help", {"--help", "-h"}, "args"); // ������塞 help
		// ����뢠�� �⤥�쭮 M N K
		opt->addOption(Options::Option(
			[opt](int, Options::Option::OptionData& data){
				(*opt)["rules"].clear();
				return (*opt)["rules"].parse({data, "nil", "nil"}, 0);
			},
			"rulesM",
			{"-m"},
			"<int>\nSet rules for M",
			1,
			[opt](Options::Option& data){
				(*opt)["rules"].clear();
				(*opt)["rules"].parse({data.getData(0), "nil", "nil"}, 0);
				return (*opt)["rules"].run();
			}
		));
		opt->addOption(Options::Option(
			[opt](int, Options::Option::OptionData& data){
				(*opt)["rules"].clear();
				return (*opt)["rules"].parse({"nil", data, "nil"}, 0);
			},
			"<int>\nrulesN",
			{"-n"},
			"Set rules for N",
			1,
			[opt](Options::Option& data){
				(*opt)["rules"].clear();
				(*opt)["rules"].parse({"nil", data.getData(0), "nil"}, 0);
				return (*opt)["rules"].run();
			}
		));
		opt->addOption(Options::Option(
			[opt](int, Options::Option::OptionData& data){
				(*opt)["rules"].clear();
				return (*opt)["rules"].parse({"nil", "nil", data}, 0);
			},
			"<int>\nrulesK",
			{"-k"},
			"Set rules for K",
			1,
			[opt](Options::Option& data){
				(*opt)["rules"].clear();
				(*opt)["rules"].parse({"nil", "nil", data.getData(0)}, 0);
				return (*opt)["rules"].run();
			}
		));
		return opt;
	}
	
	GLifeDriver::pOptions GLifeDriver::getOptConsole(){
		pOptions opt = newOptions(this->obj.getRules());
		opt->addOption(Options::Option( // ------------------------------- out
			"out",
			{},
			"Out the table",
			0,
			[this]
			(Options::Option& data)
			{
				auto matrix = this->obj.getMatrix();
				int i, j;
				std::cout << "Matrix >\n";
				for(i = 0; i < matrix.getSizeY(); i++){
					for(j = 0; j < matrix.getSizeX(); j++){
						if(matrix.get(j, i) > 0)
							std::cout << "\xB2";
						else
							std::cout << "\xD8";
					}
					std::cout << "\n";
				}
				std::cout << "--------------\n";
				return "";
			}
		));
		opt->addOption(Options::Option( // ------------------------------------- gui
			[](int, Options::Option::OptionData& data){
				if(std::string(data) == "nil")
					return "no file";
				return "";
			},
			"gui",
			{"gui"},
			"Start GUI",
			0,
			[this](Options::Option&){
				if(GLifeF::GUIopt["assign"].isOption())
					std::cout << "GUI already run. Use 'exit' for return\n";
				else {
					this->goGUI();
					std::cout << "MAGIC\n"; // �� ���窠 �� �믮������
				}
				return "";
			}
		));
		opt->addOption(Options::Option( // ------------------------------------------- load
			[](int, Options::Option::OptionData& data){
				if(std::string(data) == "nil")
					return "no file";
				return "";
			},
			"loadf",
			{"load"},
			"<file>\nLoad file with table state",
			1,
			[this](Options::Option& opt){
				auto res = this->loadFile(opt.getData(0));
				if(res.size() > 0)
					return res;
				std::cout << "File loaded\n";
				return std::string("");
			}
		));
		opt->addOption(Options::Option( // ------------------------------------------- save
			[](int, Options::Option::OptionData& data){
				if(std::string(data) == "nil")
					return "no file";
				return "";
			},
			"savef",
			{"save"},
			"<file>\nSave file with table state",
			1,
			[this](Options::Option& opt){
				auto res = this->saveFile(opt.getData(0));
				if(res.size() > 0)
					return res;
				std::cout << "File saved\n";
				return std::string("");
			}
		));
		this->addHelp(opt, "help", {"help"}, "console");
		opt->addOption(Options::Option({"helpF"},
			(*this->getOptFile())["helpF"]
		));
		return opt;
	}
	
	GLifeDriver::pOptions GLifeDriver::getOptGUI(){
		pOptions opt = newOptions(this->obj.getRules());
		opt->addOption(Options::Option( // ------------------------------- ASSIGN
			"assign",
			{},
			"Assign the game state",
			0,
			[this](Options::Option&){
				auto matrix = this->obj.getMatrix();
				GLifeF::cubes << matrix;
				return "";
			}
		));
		opt->addOption(Options::Option( // ------------------------------- CONSOLE
			"console",
			{},
			"Start console",
			0,
			[this](Options::Option&){
				this->goConsole();
				GLifeF::GUIopt["assign"]();
				return "";
			}
		));
		return opt;
	}
	
	GLifeDriver::pOptions GLifeDriver::getOptFile(){
		pOptions opt = newOptions(this->obj.getRules());
		pOptions realOpt = newOptions();
		realOpt->addOption(Options::Option({"RULES"}, (*opt)["rules"]));
		realOpt->addOption(Options::Option({"FIELD"}, (*opt)["field"]));
		realOpt->addOption(Options::Option({"SET"}, (*opt)["set"]));
		this->addHelp(realOpt, "helpF", {}, "data file structure");
		return realOpt;
	}
}
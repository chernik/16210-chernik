#ifndef OPT_H
#define OPT_H

/*
== [[opt.h]]
���०�� ����� <<Options>> � ᯮᮡ� ࠡ��� ��� ���
*/

#include <unordered_map>
#include <string>
#include <vector>
#include <memory>
#include <algorithm>

//#include <iostream> // ???

namespace Chernik {
	
	/*
	== [[Options]]
	�������� ��権
	*/
	class Options {
	public:
		class Option; // �⤥�쭠� ����
		class Parser; // �����
		class OptionError; // �訡��

		Options();

		Options(const Options&);
		Options& operator=(const Options&);

		/*
		== [[Options::addOption()]]
		�������� ����� ���� � ��������, �᫨ �� ��� ��� 䫠�� �� ���ᥪ����� � 㦥 ���������묨 � �������� ���ﬨ
		� ��⨢��� ��砥 ������ �᪫�祭�� Options::OptionError(<���ᠭ�� �訡��>)
		�訡�� ⠪�� ����頥��� �� ����� ��� ���⮩ ��ப�
		*/
		void addOption(Option);

		/*
		== [[Options::getNameFromFlag()]]
		��室�� ���� �� �� 䫠�� � �����頥� �� ���
		�᫨ ���� �� �������, �����頥� ������ ��ப�
		*/
		std::string getNameFromFlag(std::string);

		/*
		== [[Options::clearArgv()]]
		���頥� ��᢮���� ��㬥��� ��権
		*/
		void clearArgv();

		/*
		== [[Options::run()]]
		����᪠�� ��樨
		���冷� ��権 �ந������
		*/
		std::string run();

		/*
		== [[Options::getConsole()]]
		�����頥� ���ᠭ�� ��� ��権 � �ந����쭮� ���浪�
		*/
		std::string getConsole();

		/*
		== [[Options[]]]
		�����頥� ���� �� �� �����
		�᫨ ��樨 � ����� ������ �� �������, �����頥��� �ᥢ������ (�. <<Option::isOption()>>)
		*/
		Option& operator[](std::string);

		virtual ~Options();
	private:
		// ��� ⠡���� ��権
		typedef std::unordered_map<std::string, std::shared_ptr<Option>, std::hash<std::string>> OptionMap;

		OptionMap arrOption; // ������ ��権
		std::shared_ptr<Option> noOption;

		bool isCollisionFlags(Option&);
	};
// ----------------------------------------------
	class Options::Option {
	public:
		class OptionData; // ����� ��樨
		typedef std::vector<std::string> VecStr; // ��� ���ᨢ� ��ப
		typedef std::vector<std::shared_ptr<OptionData>> VecOptionData; // ��� ���ᨢ� ������
		typedef std::function<std::string(int, OptionData&)> lmbd; // ��������
		typedef std::function<std::string(Option&)> CBfunc; // �믮�����

		/*
		== [[Option()]]
		���������� ��権
		Option(<�����>)
		������� �⠭������ ����
		<�����> (name, flags, description, runner)
		* *name*        - ��� ��樨
		* *flags*       - 䫠�� ��樨 (����� ��ப)
		* *description* - ���ᠭ�� ��樨
		* *runner*      - �믮�����
		                  �ﬡ�� �㭪�� � ��㬥�⠬� Option& (�. <<Option::run()>>)
		                  � ��砥 �ᯥ� �믮������ ������ ������ ������ ��ப�
		                  ���� ������ ���ᠭ�� �訡��
		Option(valid, <�����>)
		* *valid*       - ��������
		                  �ﬡ�� �㭪�� � ��㬥�⠬� int, Option::OptionData& (�. <<Option::validate()>>)
		                  int - ������ ��㬥�� (� ���)
		                  Option::OptionData& - �. <<OptionData>>
		                  � ��砥 �ᯥ� ������ ������ ������ ��ப�
		                  ���� ������ ���ᠭ�� �訡��
		Option(flags, Option)
		������� ����� ��㣮� ��樨 � ��㣨�� 䫠����
		Option()
		������� �ᥢ������ (�. <<Option::isOption()>>)
		*/
		Option(lmbd, std::string, const VecStr&, std::string, unsigned int, CBfunc); // ��������, ���, 䫠��, ���ᠭ�� � ������⢮ ��ࠬ��஢
		Option(std::string, const VecStr&, std::string, unsigned int, CBfunc); // ���, 䫠��, ���ᠭ�� � ������⢮ ��ࠬ��஢
		Option(const VecStr&, const Option&); // ���� 䫠�� � ��������� ����
		Option(); // �ᥢ������

		Option(const Option&); // ����஢����
		Option& operator=(const Option&); // ��ᢠ������

		/*
		== [[Option()()]]
		��뢠�� �믮����� � ��樨 ��� ��㬥�⮢
		�᫨ ���� ����� ��㬥���, ����室��� ������ ��᢮��� � ������஢��� �� ��㬥���
		� ��⨢��� ��砥 ������� �᪫�祭�� Options::OptionError(<���ᠭ��>)
		�����頥� �, �� ���� �믮�����
		*/
		std::string operator()();
		
		/*
		== [[Option::getName()]]
		�����頥� ��� ��樨
		*/
		std::string getName();
		
		/*
		== [[Option::isFlag()]]
		�஢���� �ਭ���������� 䫠�� ��樨
		*/
		bool isFlag(std::string);
		
		/*
		== [[Option::getCaption()]]
		�����頥� ���ᠭ�� ��樨
		*/
		std::string getCaption();
		
		/*
		== [[Option::pushData()]]
		�������� ��।��� ��㬥�� <<OptionData>> � ����
		*/
		void pushData(const OptionData&);
		
		/*
		== [[Option::getData()]]
		�� ������⢨� ��ࠬ��஢ �����頥� �����塞� ᯨ᮪ ��㬥�⮢
		�� ������⢨� ������ int ��ࠬ��� �����頥� ᮮ⢥�����騩 ������� ��㬥��
		�� ������⢨� ��㬥�� � ����� �����ᮬ ����뢠���� �᪫�祭�� Options::OptionError(<���ᠭ��>)
		*/
		VecOptionData& getData();
		OptionData& getData(unsigned int);
		
		/*
		== [[Option::isOption()]]
		������� �� ���� �ᥢ����樥�
		�ᥢ������ �ᯮ������ � ��砥 ������������ �ᯮ�짮���� ���������� ����
		�᪫�祭�� � ⠪�� ��砥 �� ����뢠����
		*/
		bool isOption();
		
		/*
		== [[Option::isFull()]]
		������஢��� �� �� ����
		*/
		bool isFull();
		
		/*
		== [[Option::getArgc()]]
		�����頥� ����室���� ������⢮ ��㬥�⮢
		*/
		unsigned int getArgc();
		
		/*
		== [[Option::validate()]]
		��������� ���� ��⥬ ��᫥����⥫쭮�� ����᪠ �������� � �����ᮬ ��।���� ��ࠬ��� � ᠬ�� ��ࠬ��஬
		�����頥� ������ ��ப� � ��砥 �ᯥ� � ��ப�, �����饭��� �������஬ � ��砥 �訡��
		*/
		std::string validate();
		
		/*
		== [[Option::console()]]
		�����頥� ���ᠭ�� ��樨, ᮤ�ঠ饥 �� ���, 䫠�� � ���ᠭ��
		*/
		std::string console();
		
		/*
		== [[Option::clear()]]
		��頥� ᯨ᮪ ��㬥�⮢
		*/
		void clear();
		
		/*
		== [[Option::run()]]
		�믮���� ������஢����� ����
		�����頥� �, �� �����頥� ��������
		���� �����頥� ������ ��ப�
		*/
		std::string run();
		
		/*
		== [[Option::parse()]]
		����� ᯨ᮪ ��㬥�⮢, ��稭�� � ����樨 ��ࢮ��, � ��������� ����
		*/
		std::string parse(VecStr, unsigned int);
		
		/*
		== [[Option::isCollision()]]
		�஢����, ���� �� �������� 䫠��� ��㣮� ��樨 � ⥪�饩
		*/
		bool isCollision(Option&);
		
		/*
		== [[Option::setIgnore()]]
		��⠭��������, �㦭� �� �����஢��� ���������� ��樨 � <<Option::parse()>>
		�������� �����஢��� ��।������ ��樨 �� ���ᨭ��
		*/
		void setIgnore(bool);
		
		/*
		== [[Option::isParseable()]]
		�஢����, ���� �� � ��樨 䫠�� ��� �� �ᯮ�짮����� � �����
		�᫨ ���� �� �����㥬�, � �� ����� �맢��� ����஬
		*/
		bool isParseable();

		virtual ~Option(); // ��������
	private:
		bool isOpt; // �� �ᥢ������?
		bool isFul; // ���������?
		bool ignore; // ������㥬?
		std::string name; // ���
		VecStr flags; // �����
		std::string caption; // ���ᠭ��
		VecOptionData data; // �����
		unsigned int argc; // ������⢮ ������
		lmbd validator; // ��������
		CBfunc callback; // ��ᢠ���⥫�
	};

	class Options::Parser {
	public:
		class Error; // �訡�� �����
		class Helper; // ����魨� �����
		typedef std::vector<std::shared_ptr<Error>> VecErr; // ��� ���ᨢ� �訡��

		/*
		== [[Parser()]]
		��易⥫�� ��㬥�� - <<Options>>
		����⠥� �� ��뫪�, �� ����砥� ������� ���:
			Options test1, test2;
			// test1 != test2
			Options::Parser p(test1);
			// &p.getOptions() == &test1
			p = Options::Parser(test2);
			// test1 == test2, ��⮬� �� ��९�ᢠ������� ��뫮筮� ����
		*/
		Parser(Options&); // ���ᨭ� �� �������騬 ����

		Parser(const Parser&); // ����஢����
		Parser& operator=(const Parser&); // ��ᢠ������

		/*
		== [[Parser::parse()]]
		����� 㪠����� ���ᨢ ��ப
		���ਬ��, ���� ���ᨢ {"--file", "input", "--iter", "6"}
		����� ���� ���ᠭ�� ��権:
			* ���� � 䫠��� "--file" � ��ப��� ��㬥�⮬ (�� 㬮�砭�� ��� ��������, �. <<Option::validate()>>)
			* ���� � 䫠��� "--iter" � �᫮�� ��㬥�⮬ (� �������� ����� ������� data.cast(OptionData::INT))
			* ���� � 䫠��� "--test"
		�� ����� �������� ������樨 � ��������� ����� ��樨 �⠭������ ���������묨 (�. <<Option::isFull()>>),
			�஬� ��樨 � 䫠��� "--test"
		*/
		void parse(Options::Option::VecStr);
		
		bool isError(); // ���� �訡��?
		
		/*
		== [[Parser::getErrors()]]
		���� �� �訡�� �����
		�� ������ �訡�� ����� �� �४�頥� ࠡ���
		*/
		VecErr& getErrors();
		
		Options& getOptions(); // ����� ��樨

		virtual ~Parser(); // ��������
	private:
		VecErr errors; // �訡��
		Options& options; // ��樨

		Options::Option::VecStr clear0str(Options::Option::VecStr);
	};
// ------------------------------------------------------------
	class Options::Option::OptionData {
	public:
		enum class Type { // ���� ������
			UNKNOWN,
			STRING,
			INT,
			BOOL
		};

		OptionData(std::string); // �����
		OptionData(int);
		OptionData(bool);

		OptionData(); // ��� ������

		OptionData(const OptionData&); // ����஢����
		OptionData& operator=(const OptionData&); // ��ᢠ������

		/*
		== [[OptionData(cast)]]
		���� �८�ࠧ������ ⨯�
		�ᯮ����� <<OptionData::cast()>>
		� ��砥 �訡�� ����뢠�� �᪫�祭�� <<OptionError>>
		*/
		operator std::string();
		operator int();
		operator bool();

		Type typeData(); // ��� ������
		
		/*
		== [[OptionData::cast()]]
		�८�ࠧ��뢠�� ⨯ � 㪠�����
		� ��砥 �訡�� �����頥� �� ���ᠭ��
		� ��砥 �ᯥ� �����頥� ������ ��ப�
		*/
		std::string cast(Type);

		virtual ~OptionData(); // ��������
	private:
		std::string dataString; // �����
		int dataInt;
		bool dataBool;
		Type type; // ���
	};

	class Options::Parser::Error {
	public:
		Error(int, std::string); // ���ᠭ��
		Error(std::string, const Options::Option&); // ���ᠭ�� � ����

		Error(const Error&); // ����஢����
		Error& operator=(const Error&); // ��ᢠ������

		std::string console(); // ���ᠭ�� �訡��

		virtual ~Error(); // ��������
	private:
		std::string caption; // ���ᠭ��
		Options::Option option; // ����
	};

	class Options::Parser::Helper {
	public:
		class String;
		class VecStr;
		Helper();

		static Options::Option::VecStr argCVparse(int, char**);
	};
// --------------------------------------------------------------------------------------
	/*
	== [[Helper::String]]
	����� std::string � ��ॣ�㦥��� �����஬ split("word")
	*/
	class Options::Parser::Helper::String : public std::string {
	public:
		using std::string::string;

		using std::string::operator=;
		using std::string::operator[];

		String(std::string);
		String();

		Options::Parser::Helper::VecStr split(String);
	};

	/*
	== [[Helper::VecStr]]
	����� std::vector<Helper::String> � ��ॣ�㦥��� �����஬ join("word")
	*/
	class Options::Parser::Helper::VecStr : public std::vector<Options::Parser::Helper::String> {
	public:
		using std::vector<Options::Parser::Helper::String>::vector;

		using std::vector<Options::Parser::Helper::String>::operator=;
		using std::vector<Options::Parser::Helper::String>::operator[];

		VecStr& operator=(const std::vector<std::string>&);
		operator std::vector<std::string>();

		Options::Parser::Helper::String join(Options::Parser::Helper::String);
	};
// ---------------------------------------------------------------------------
	class Options::OptionError : public std::exception {
	public:
		using std::exception::exception;
		using std::exception::operator=;

		OptionError(std::string);

		OptionError(const OptionError&);
		OptionError& operator=(const OptionError&);

		std::string what();
	private:
		std::string wht;
	};
}

#endif
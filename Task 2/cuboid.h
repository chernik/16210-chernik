#ifndef CUBOID_H
#define CUBOID_H
/*
== stuff.h
�����প� ����᪮�� �뢮��  GLife � OpenGL
*/

#include "initgl.h"
#include "stuff.h"

namespace Chernik {

	/*
	== [[MatrixCube]]
	����� ��ꥪ⮢ <<MatrixCube::Cube>> � ࠡ�� � ����
	�।�⠢��� ᮡ�� ������ 3D �㡮� � ���� <<Matrix>> ⨯� <int>.
	*/
	class MatrixCube {
	public:
		class Cube;
		
		MatrixCube();
		
		MatrixCube(const MatrixCube&);
		MatrixCube& operator=(const MatrixCube&);
		
		/*
		== [[MatrixCube::out()]]
		�뢮� � OpenGL
		*/
		void out();
		
		/*
		== [[MatrixCube<<]]
		���� ������ <<GLife::getMatrix()>>
		*/
		MatrixCube& operator<<(Matrix<int>& matrix);
		
		virtual ~MatrixCube();
	private:
		friend class Cube;
		
		Matrix<Cube> matrix;
		float sizeX, sizeY;
		float hL, hH;
		std::vector<float> color;
	};
	
	/*
	== [[MatrixCube::Cube]]
	������� <<MatrixCube>>
	*/
	class MatrixCube::Cube {
	public:
		Cube();
		
		Cube(const Cube&);
		Cube& operator=(const Cube&);
		
		/*
		== [[Cube::out()]]
		�뢮� �⤥�쭮�� �������
		����� ���ᠭ ᯮᮡ �뢮�� ������ <<Matrix>> ⨯� <int> � 3D � ������� OpenGL
		�ॡ�� த�⥫� <<MatrixCube>>
		*/
		void out(MatrixCube&, int, int);
		
		/*
		== [[Cube::set()]]
		��⠭���� ���ﭨ� �㡠
		*/
		void set(int);
		
		virtual ~Cube();
	private:
		int res;
	};
}

#endif
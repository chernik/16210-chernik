#include "opt.h"

#include <iostream>

namespace Chernik {

// ------------------------------------------------------------------------------------------- OPTIONS
	Options::Options():
		noOption(new Option()){};
// -------------
	Options::Options(const Options& other):
			noOption(new Option()){
		this->arrOption = other.arrOption;
	}
	
	Options& Options::operator=(const Options& other){
		this->arrOption = other.arrOption;
		return *this;
	}
// ----------
	void Options::addOption(Option option){
		if(option.getName() == "")
			throw OptionError("Empty name at Options::addOption()");
		if((*this)[option.getName()].isOption())
			throw OptionError("Duplicate names at Options::addOption()");
		if(this->isCollisionFlags(option))
			throw OptionError("Duplicate flags at Options::addOption()");
		this->arrOption.insert({{
			option.getName(),
			std::make_shared<Option>(option)
		}});
	}
	
	std::string Options::getNameFromFlag(std::string flag){
		for(auto&& iter : this->arrOption){
			if(iter.second->isFlag(flag))
				return iter.second->getName();
		}
		return "";
	}
	
	void Options::clearArgv(){
		for(auto&& iter : this->arrOption){
			iter.second->clear();
		}
	}
	
	std::string Options::run(){
		std::string res;
		for(auto&& iter : this->arrOption){
			res = iter.second->run();
			if(res.size() > 0)
				return res;
		}
		return res;
	}
	
	std::string Options::getConsole(){
		std::string str;
		for(auto&& iter : this->arrOption){
			if(iter.second->isParseable())
				str += iter.second->console();
		}
		return str;
	}
// -------------
	Options::Option& Options::
			operator[](std::string optionName){
		auto fnd = this->arrOption.find(optionName);
		if(fnd == this->arrOption.end())
			return *(this->noOption);
		else
			return *(fnd->second);
		
	}
// -----------------------
	Options::~Options(){};
// -----------------------
	bool Options::isCollisionFlags(Option& opt){
		for(auto&& iter : this->arrOption)
			if(iter.second->isCollision(opt))
				return true;
		return false;
	}
// ------------------------------------------------------------------------------- OPTIONS::OPTION
	Options::Option::Option(
			lmbd validator,
			std::string optName,
			const VecStr& optFlags,
			std::string optCaption,
			unsigned int argc,
			CBfunc cb) :
			isOpt(true),
			isFul(false),
			ignore(false),
			name(optName),
			flags(optFlags),
			caption(optCaption),
			argc(argc),
			validator(validator),
			callback(cb) {};
	
	Options::Option::Option(
			std::string optName,
			const VecStr& optFlags,
			std::string optCaption,
			unsigned int argc,
			CBfunc cb) :
			isOpt(true),
			isFul(false),
			ignore(false),
			name(optName),
			flags(optFlags),
			caption(optCaption),
			argc(argc),
			validator([](int, OptionData&){return "";}),
			callback(cb) {};
	
	Options::Option::Option(const VecStr& flags, const Option& other){
		this->isOpt	= other.isOpt;
		this->isFul	= other.isFul;
		this->ignore	= other.ignore;
		this->name	= other.name;
		this->flags	= flags;
		this->caption	= other.caption;
		this->data	= other.data;
		this->argc	= other.argc;
		this->validator	= other.validator;
		this->callback	= other.callback;
	}
	
	Options::Option::Option(): 
		isOpt(false), isFul(false){};
// ----------
	Options::Option::Option(const Option& other){
		this->isOpt	= other.isOpt;
		this->isFul	= other.isFul;
		this->ignore	= other.ignore;
		this->name	= other.name;
		this->flags	= other.flags;
		this->caption	= other.caption;
		this->data	= other.data;
		this->argc	= other.argc;
		this->validator	= other.validator;
		this->callback	= other.callback;
	}
	
	Options::Option& Options::Option::operator=(const Option& other){
		this->isOpt	= other.isOpt;
		this->isFul	= other.isFul;
		this->ignore	= other.ignore;
		this->name	= other.name;
		this->flags	= other.flags;
		this->caption	= other.caption;
		this->data	= other.data;
		this->argc	= other.argc;
		this->validator	= other.validator;
		this->callback	= other.callback;
		return *this;
	}
// -------------------
	std::string Options::Option::operator()(){ // to add
		if(this->argc > 0)
			throw Options::OptionError("Option can't runnable");
		std::string res = this->validate();
		if(res.size() > 0)
			throw Options::OptionError("In runnable function error: <" + res + ">");
		if(!this->isOpt)
			throw Options::OptionError("Option is no option");
		res = this->run();
		this->clear();
		return res;
	}
// -----------------
	std::string Options::Option::getName(){
		return this->name;
	}
	
	bool Options::Option::isFlag(std::string flag){
		auto fnd = std::find(
			this->flags.begin(),
			this->flags.end(),
			flag);
		return fnd != this->flags.end();
	}
	
	std::string Options::Option::getCaption(){
		return this->caption;
	}
	
	void Options::Option::pushData(const OptionData& data){
		this->data.push_back(std::make_shared<OptionData>(data));
	}
	
	Options::Option::VecOptionData& Options::Option::getData(){
		return this->data;
	}
	
	Options::Option::OptionData& Options::Option::getData(unsigned int i){
		if(i < this->data.size())
			return *(this->data[i]);
		else
			throw Options::OptionError("OptionData doesn't exist");
	}
	
	bool Options::Option::isOption(){
		return this->isOpt;
	}
	
	bool Options::Option::isFull(){
		return this->isFul;
	}
	
	unsigned int Options::Option::getArgc(){
		return this->argc;
	}
	
	std::string Options::Option::validate(){
		unsigned int i;
		for(i = 0; i < this->data.size(); i++){
			std::string res = this->validator(i, this->getData(i));
			if(res.size() > 0){
				this->data.clear();
				return res;
			}
		}
		this->isFul = true;
		return "";
	}
	
	std::string Options::Option::console(){
		std::string str;
		str += "Option";
		str += " <" + this->name + ">\n";
		str += "|||||\n";
		str += "Flags:";
		for(auto&& iter : this->flags){
			str += " " + iter;
		}
		str += "\n";
		str += this->caption + "\n";
		str += "-----\n";
		return str;
	}
	
	void Options::Option::clear(){
		this->data.clear();
		this->isFul = false;
	}
	
	std::string Options::Option::run(){
		if(this->isFull()){
			return this->callback(*this);
		}
		return "";
	}
	
	std::string Options::Option::parse(VecStr argv, unsigned int ind){
		if(argv.size() < ind + this->getArgc()) // ��墠⪠ ��㬥�⮢
			return "No arguments";
			unsigned int i;
		for(i = 0; i < this->getArgc(); i++){
			this->pushData(argv[ind + i]); // ����ࠥ� ��ࠬ���� ��ப����� ⨯�
		}
		if(!this->ignore)
			return this->validate(); // ��ॢ���� � �㦭� ⨯
		else
			return "";
	}
	
	bool Options::Option::isCollision(Option& other){
		for(auto&& iter : other.flags)
			if(this->isFlag(iter))
				return true;
		return false;
	}
	
	void Options::Option::setIgnore(bool state){
		this->ignore = state;
	}
	
	bool Options::Option::isParseable(){
		return this->flags.size() > 0;
	}
// --------------
	Options::Option::~Option(){};
// ------------------------------------------------------------------------------------ OPTIONS::PARSER
	Options::Parser::Parser(Options& options) :
			options(options) {};
// --------------
	Options::Parser::Parser(const Parser& other) : options(other.options){
		this->errors = other.errors;
	}
	
	Options::Parser& Options::Parser::operator=(const Parser& other){
		this->options = other.options;
		this->errors = other.errors;
		return *this;
	}
// -------------
	void Options::Parser::parse(Options::Option::VecStr argv){
		unsigned int i;
		this->errors.clear(); // ���ࠥ� �訡��
		this->options.clearArgv(); // �����蠥� ��樨
		argv = this->clear0str(argv);
		for(i = 0; i < argv.size(); i++){ // ��ॡ�� ��㬥�⮢
			std::string opt = // ��室�� ��� ��樨 �� 䫠��
				this->options.getNameFromFlag(argv[i]);
			if(opt.size() > 0){ // �᫨ ��� ����
				std::string res = "";
				if(this->options[opt].isFull())
					res = "Duplicate option";
				else
					res = // ���ᨬ // ?? ��।����� ��� �����, ��������
				  		this->options[opt].parse(argv, i+1);
				if(res.size() > 0) // �᫨ ���� �訡��
					this->errors.push_back(
						std::make_shared<Error>(
							res, this->options[opt]
					));
				  i += this->options[opt].getArgc(); // ���᪮� �१ ��㬥���
			} else {
				this->errors.push_back( // ���������� ����
					std::make_shared<Error>(0,
						"Unknown node '" + argv[i] + "'"
				));
			}
		}
	}
	
	bool Options::Parser::isError(){
		return this->errors.size() > 0;
	}
	
	Options::Parser::VecErr& Options::Parser::getErrors(){
		return this->errors;
	}
	
	Options& Options::Parser::getOptions(){
		return this->options;
	}
// --------------
	Options::Parser::~Parser(){};
// --------------
	Options::Option::VecStr Options::Parser::clear0str(Options::Option::VecStr arrIn){
		Options::Option::VecStr arrOut;
		for(auto&& iter : arrIn)
			if(iter.size() > 0)
				arrOut.push_back(iter);
		return arrOut;
	}
// ---------------------------------------------------------------------------------- OPTIONS::OPTION::OPTIONDATA
	Options::Option::OptionData::
		OptionData(std::string data):
			dataString(data),
			type(Type::STRING){};
	
	Options::Option::OptionData::
		OptionData(int data):
			dataInt(data),
			type(Type::INT){};
	
	Options::Option::OptionData::
		OptionData(bool data):
			dataBool(data),
			type(Type::BOOL){};
	
	Options::Option::OptionData::
		OptionData():
			type(Type::UNKNOWN){};
// --------------
	Options::Option::OptionData::
		OptionData(const OptionData& other){
			this->dataString
				= other.dataString;
			this->dataInt
				= other.dataInt;
			this->dataBool
				= other.dataBool;
			this->type
				= other.type;
	}
	
	Options::Option::OptionData&
	Options::Option::OptionData::
		operator=(const OptionData& other){
			this->dataString
				= other.dataString;
			this->dataInt
				= other.dataInt;
			this->dataBool
				= other.dataBool;
			this->type
				= other.type;
			return *this;
	}
// -----------
/*
	Options::Option::OptionData&
	Options::Option::OptionData::
		operator=(std::string data){
			(*this) = OptionData(data);
	}
	
	Options::Option::OptionData&
	Options::Option::OptionData::
		operator=(int data){
			(*this) = OptionData(data);
	}
	
	Options::Option::OptionData&
	Options::Option::OptionData::
		operator=(bool data){
			(*this) = OptionData(data);
	}
*/
// ------------
	Options::Option::OptionData::
		operator std::string(){
			Type tp = Type::STRING;
			if(this->type != tp){
				std::string res = this->cast(tp);
				if(res.size() > 0)
					throw Options::OptionError("Wrong cast at Options::Option::OptionData: " + res);
			}
			return this->dataString;
	}
	
	Options::Option::OptionData::
		operator int(){
			Type tp = Type::INT;
			if(this->type != tp){
				std::string res = this->cast(tp);
				if(res.size() > 0)
					throw Options::OptionError("Wrong cast at Options::Option::OptionData: " + res);
			}
			return this->dataInt;
	}
	
	Options::Option::OptionData::
		operator bool(){
			Type tp = Type::BOOL;
			if(this->type != tp){
				std::string res = this->cast(tp);
				if(res.size() > 0)
					throw Options::OptionError("Wrong cast at Options::Option::OptionData: " + res);
			}
			return this->dataBool;
	}
// -----------
	Options::Option::OptionData::Type Options::Option::OptionData::typeData(){
		return this->type;
	}
	
	std::string Options::Option::OptionData::cast(Type type){
		if(this->type == type)
			return "";
		if(this->type == Type::UNKNOWN)
			return "convert UNKNOWN to other";
		if(type == Type::UNKNOWN){
			this->type = Type::UNKNOWN;
			return "";
		}
		if(type == Type::STRING){
			switch(this->type){
			case Type::INT:
				this->dataString = std::to_string(this->dataInt);
				break;
			case Type::BOOL:
				if(this->dataString[0] == 'T' || this->dataString[0] == 't')
					this->dataBool = true;
				else
					this->dataBool = false;
				break;
			default:
				return "conver from unknown type";
			}
			this->type = Type::STRING;
			return "";
		}
		std::string res = this->cast(Type::STRING);
		if(res.size() > 0)
			return res;
		std::string::size_type t = 0;
		switch(type){
		case Type::INT:
			try{
				this->dataInt = std::stoi(this->dataString, &t);
			}catch(std::invalid_argument err){
				return "convert no-number to number";
			}
			if(t < this->dataString.size())
				return "conver no-number to number";
			break;
		case Type::BOOL:
			if(this->dataBool)
				this->dataString = "True";
			else
				this->dataString = "False";
			break;
		default:
			return "convert to unknown type";
		}
		this->type = type;
		return "";
	}	
// -------------
	Options::Option::OptionData::
		~OptionData(){};
// ---------------------------------------------------------------------------------------- OPTIONS::PARSER::ERROR
	Options::Parser::Error::
	Error(std::string capt, const Options::Option& opt){
		this->caption = capt;
		this->option = opt;
	}
	
	Options::Parser::Error::Error(int, std::string capt){
		this->caption = capt;
	}
// ----------
	Options::Parser::Error::
	Error(const Error& other){
		this->caption = other.caption;
		this->option = other.option;
	}
	
	Options::Parser::Error&
	Options::Parser::Error::
	operator=(const Error& other){
		this->caption = other.caption;
		this->option = other.option;
		return *this;
	}
// -------
	std::string Options::Parser::Error::console(){
		std::string str;
		str += "Parser error";
		str += " <" + this->caption + ">\n";
		if(this->option.isOption()){
			str += "   at option:\n";
			str += this->option.console();
		}
		return str;
	}
// -------
	Options::Parser::Error::
		~Error(){};
// ----------------------------------------------------------- OPTIONS::PARSER::HELPER
	Options::Parser::Helper::Helper(){};
	
	Options::Option::VecStr
	Options::Parser::Helper::
	argCVparse(int argc, char** argv){
		Options::Option::VecStr arr;
		int i;
		for(i = 1; i < argc; i++)
			arr.push_back(argv[i]);
		return arr;
	}
// ----------------------------------------------------------- OPTIONS::PARSER::HELPER::VECSTR
	Options::Parser::Helper::VecStr::
	operator std::vector<std::string>(){
		std::vector<std::string> arr(this->size());
		size_t i;
		for(i = 0; i < this->size(); i++)
			arr[i] = (*this)[i];
		return arr;
	}
	
	Options::Parser::Helper::VecStr&
	Options::Parser::Helper::VecStr::operator=(const std::vector<std::string>& other){
		VecStr arr(other.size());
		size_t i;
		for(i = 0; i < other.size(); i++)
			arr[i] = other[i];
		*this = arr;
		return *this;
	}
	
	Options::Parser::Helper::String
	Options::Parser::Helper::VecStr::
	join(Options::Parser::Helper::String str){
		Helper::String res = "";
		size_t i;
		for(i = 0; i < this->size(); i++){
			if(i > 0)
				res += str;
			res += (*this)[i];
		}
		return res;
	}	
// --------------------------------------------------------------------------------- OPTIONS::PARSER::HELPER::STRING
	Options::Parser::Helper::String::String(){};
	
	Options::Parser::Helper::String::
	String(std::string string){
		std::string& str = *this;
		str = string;
	}
	
	Options::Parser::Helper::VecStr
	Options::Parser::Helper::String::
	split(Options::Parser::Helper::String str){
		Helper::VecStr arr;
		Helper::String cup;
		int pos = 0;
		while(true){
			pos = this->find(str);
			if(pos < 0){ // �᫨ �� ��襫, � -1
				arr.push_back(*this);
				break;
			}
			cup = this->substr(0, pos);
			arr.push_back(cup);
			(*this) = this->substr(pos + str.size());
		}
		return arr;
	}
// ------------------------------------------------------------------------------------- Options::OptionError
	Options::OptionError::OptionError(const OptionError& other){
		this->wht = other.wht;
	}
	
	Options::OptionError& Options::OptionError::operator=(const OptionError& other){
		this->wht = other.wht;
		return *this;
	}
	
	Options::OptionError::OptionError(std::string err){
		this->wht = err;
	}
	
	std::string Options::OptionError::what(){
		return this->wht;
	}
// ------------------------------------------------------------------------------------------------ ALL
};
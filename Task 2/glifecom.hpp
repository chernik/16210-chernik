// ��� 䠩� ᮤ�ন� ���ᠭ�� �㭪権 ��� Options
//namespace Chernik {
	// ------------------------------------------------------------------------ ��� ���᮫�
	Options GLife::getRules(){
		Options opt;
		// ------------------------------------------------------------------------ reset
		opt.addOption(Options::Option(
			"reset",
			{"reset"},
			"Reset the table",
			0,
			[this](Options::Option&){
				this->doubleState();
				this->states.back()->matrix.clear();
				return "";
			}
		));
		// ------------------------------------------------------------------------ set X Y
		opt.addOption(Options::Option(
			[](int,
			Options::Option::
			OptionData& data){
				std::string err = data.cast(Options::Option::OptionData::Type::INT);
				if(err.size() > 0)
					return err;
				if(int(data) < 0)
					return std::string("Negative index");
				return std::string("");
			},
			"set",
			{"set"},
			"<int> <int>\nSet life at the X Y",
			2,
			[this](Options::Option& opt){
				this->doubleState();
				if(this->states.back()->X.get() <= int(opt.getData(0)))
					return "Wrong X size";
				if(this->states.back()->Y.get() <= int(opt.getData(1)))
					return "Wrong Y size";
				this->states.back()->matrix.set(
					opt.getData(0),
					opt.getData(1),
					1);
				return "";
			}
		));
		// ------------------------------------------------------------------------ clear X Y
		opt.addOption(Options::Option(
			[](int,
			Options::Option::
			OptionData& data){
				std::string err = data.cast(Options::Option::OptionData::Type::INT);
				if(err.size() > 0)
					return err;
				if(int(data) < 0)
					return std::string("Negative index");
				return std::string("");
			},
			"clear",
			{"clear"},
			"<int> <int>\nClear the life at X Y",
			2,
			[this](Options::Option& opt){
				this->doubleState();
				this->states.back()->matrix.set(
					opt.getData(0),
					opt.getData(1),
					0);
				return "";
			}
		));
		// ----------------------------------------------------------------------------- one step
		opt.addOption(Options::Option(
			"stepone",
			{},
			"Step 1 times",
			0,
			[this](Options::Option&){
				GLifeState prev = *this->states.back();
				auto nX = [&prev](int x){
					return (x + prev.matrix.getSizeX()) % prev.matrix.getSizeX();
				};
				auto nY = [&prev](int y){
					return (y + prev.matrix.getSizeY()) % prev.matrix.getSizeY();
				};
				this->doubleState();
				int i, j;
				for(i = 0; i < this->states.back()->matrix.getSizeX(); i++)
				for(j = 0; j < this->states.back()->matrix.getSizeY(); j++){
					int count = 0;
					count += prev.matrix.get(nX(i - 1), nY(j - 1));
					count += prev.matrix.get(nX(i - 1), nY(j + 0));
					count += prev.matrix.get(nX(i - 1), nY(j + 1));
					count += prev.matrix.get(nX(i - 0), nY(j - 1));
					count += prev.matrix.get(nX(i - 0), nY(j + 1));
					count += prev.matrix.get(nX(i + 1), nY(j - 1));
					count += prev.matrix.get(nX(i + 1), nY(j + 0));
					count += prev.matrix.get(nX(i + 1), nY(j + 1));
					if(count == prev.M.get())
						this->states.back()->matrix.set(i, j, 1);
					if(count < prev.N.get() || count > prev.K.get())
						this->states.back()->matrix.set(i, j, 0);
				}
				return "";
			}
		));
		// ----------------------------------------------------------------------- empty command
		opt.addOption(Options::Option(
			"nil",
			{"nil"},
			"Empty command or argument",
			0,
			[](Options::Option&){return "";}
		));
		// ----------------------------------------------------------------------- step N times
		opt.addOption(Options::Option(
			[](int,
			Options::Option::
			OptionData& data){
				if(std::string(data) == "nil"){
					data = int(1);
					return std::string("");
				}
				std::string err = data.cast(Options::Option::OptionData::Type::INT);
				if(err.size() > 0)
					return err;
				if(int(data) < 0)
					return std::string("Negative step time");
				return std::string("");
			},
			"step",
			{"step"},
			"<int>\nStep N times",
			1,
			[this](Options::Option& opt){
				int i;
				this->doubleState();
				bool bufBack = this->isBack;
				this->isBack = false;
				for(i = 0; i < opt.getData(0); i++)
					this->getRules()["stepone"]();
				this->isBack = bufBack;
				return "";
			}
		));
		// ------------------------------------------------------------------------ back
		opt.addOption(Options::Option(
			"back",
			{"back"},
			"Cancel the last operation",
			0,
			[this](Options::Option&){
				if(this->states.size() > 1)
					this->states.pop_back();
				else
					return "No back";
				return "";
			}
		));
		// --------------------------------------------------------------------------- rules
		opt.addOption(Options::Option(
			[this](int ind, Options::Option::OptionData& data){
				if(std::string(data) == "nil")
					switch(ind){
					case 0:
						data = int(this->states.back()->M.get());
						break;
					case 1:
						data = int(this->states.back()->N.get());
						break;
					case 2:
						data = int(this->states.back()->K.get());
						break;
					default:
						break;
					}
				std::string err = data.cast(Options::Option::OptionData::Type::INT);
				if(err.size() > 0)
					return err;
				if(int(data) < 0)
					return std::string("Negative value of rules");
				return std::string("");
			},
			"rules",
			{"rules"},
			"<int> <int> <int>\nSet the new rules (M N K)",
			3,
			[this](Options::Option& opt){
				this->states.back()->M.set(opt.getData(0));
				this->states.back()->N.set(opt.getData(1));
				this->states.back()->K.set(opt.getData(2));
				return "";
			}
		));
		// -------------------------------------------------------------------------------- field
		opt.addOption(Options::Option(
			[](int, Options::Option::OptionData& data){
				std::string err = data.cast(Options::Option::OptionData::Type::INT);
				if(err.size() > 0)
					return err;
				if(int(data) < 0)
					return std::string("Negative value of rules");
				return std::string("");
			},
			"field",
			{"field"},
			"<int> <int>\nSet the table size",
			2,
			[this](Options::Option& opt){
				this->states.back()->matrix.resize(
					opt.getData(0),
					opt.getData(1));
				this->states.back()->X.set(opt.getData(0));
				this->states.back()->Y.set(opt.getData(1));
				return "";
			}
		));
		// ------------------------
		return opt;
	}
//}
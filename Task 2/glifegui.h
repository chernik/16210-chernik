#ifndef GLIFEGUI_H
#define GLIFEGUI_H

#include "initgl.h" // OpenGL
#include "cuboid.h" // MatrixCube
#include "opt.h" // Options

namespace Chernik {
	namespace GLifeF { // ��ࠡ��稪 GUI
		extern GLdriver drv; // ��ࠢ����� OpenGL
		extern MatrixCube cubes; // �࠭���� ���ﭨ�
		extern Options GUIopt; // �࠭���� Options ��� GUI
		
		// ������
		void draw(); // ���ᮢ��
		void move(int, int); // ��६�饭�� ���
		void roll(int, int, int, int); // ����⨥ ������ ���
		void key(unsigned char, int, int); // ����⨥ �� ���������
		//void vis(int); // ???
		
		void init(Options&); // ��⠭���� GUI � Options
	}
}

#endif
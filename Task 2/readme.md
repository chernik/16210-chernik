����� �஥�� ॠ����� ���� "�����".

��������:
	1. ����� (opt.h)
	����� ��樨 ���� {"<flag1>", "<arg1>", "<arg2>", "<flag2>", "<arg1>", "<arg2>", ...}.
--------------------------------------------------------------------------------------------------------------------------
	Options() - ����� ��権.
		.addOption(Option) - �������� ���� � ������. �᫨ � ���:
			�) ���� �� 䫠��� ᮮ⢥����� 㦥 ����������� ��樨 ("<error>" = "duplicate flags")
			�) ��� ᮮ⢥����� ����� 㦥 ����������� ��樨 ("<error>" = "duplicate names")
			�) ��� ���� ("<error>" = "empty name")
			� ������� �᪫�祭�� Options::OptionError("<error> at Options.addOption()"). // to add
		.getNameFromFlag(std::string flag = "<flag1>") - ��� ��� ��樨 �� 䫠��, ����� �� ᮮ⢥�����.
		.clearArgv() - ���뢠�� ��᢮���� ���㬥��� � ��� ��権.
		run() - �믮���� ��樨.
		["<name>"] - ��� ���� �� �� �����. � ��砥 ������⢨� �������� ��樨 
	Options::Option(<data>) - �����筠� ����.
		<data> - ����� ��ࠬ��஢ ��樨 (�� ���浪�):
			�) [](int index, Opitons::Option::OptionData& data){} - �ﬡ�� �������� (����易⥫쭮).
				index - ������ ��㬥�� ("<arg1>" => 0 � �. �.).
				data - �����. ����� �࠭��� ࠧ���� �����, ���஡��� Options::Option::OptionData, �. ����.
			�������� �㦨� ��� �।�஢�થ ������ �� �஢�� ���ᨭ��, � �� �믮������.
			�����頥� ������ ��ப� � ��砥 �ᯥ�, � ��ப� "<error_description>"
				� ���᭥���� � ��砥 �訡��. ��. Options::Parser::Error.
			� ��砥 ������⢨� ����� �� �஢�������.
			�) "<name>" - ��� ��樨.
			�) std::vector<std::string> {"<flag1>", "<flag2>", ...} - 䫠�� ��樨.
			�) "<description>" - ���ᠭ��. ����� ���� �����, �� �� ������⢮����.
			�) unsigned int argc - ������⢮ ��㬥�⮢.
			�) [](Options::Option& opt){} - �ﬡ�� �믮���⥫�.
				opt - ����, � ���ன ��室���� �믮���⥫�.
			�믮������ �� �믮������ ��樨.
		<data> ���� - ᮧ������ �ᥢ������ (�. .isOption() ����).
		.getName() - �����頥� ��� �㭪樨.
		bool .isFlag("<flag1>") - �஢����, ����᪠�� �� ������ ���� ����� 䫠�.
		.setFlags({"<flag1>", "<flag2>", ...}) - ������� �����⨬� 䫠��. // to add
		.getCaption() - �����頥� ���ᠭ��.
		.pushData(Options::Option::OptionData&) - �������� ��।��� ��㬥��.
		.getData() - �����頥� ��뫪� �� �� ��㬥���.
		.getData(int index) - �����頥� ��뫪� �� �⤥��� ��㬥��.
			�� ������⨨ ������ ����뢠���� �᪫�祭�� OptionError("Range error at Options.getData()").
		bool .isOption() - ���� �� ���� �ᥢ����樥� (�. ���).
		bool .isFull() - ��������� �� ���� (�. ����).
		.getArgc() - �����頥� ������⢮ ��㬥�⮢.
		std::string validate() - ����᪠�� �������� (�. ���) ��� ������� ��㬥��.
			��㬥��� �஢������� �� ���浪�. �� ��ࢮ� �訡�� �������� �४�頥��� � ��ப� �訡�� �����頥���.
			�� �ᯥ譮� ������樨 ��� ��㬥�⮢ ���� �⠭������ ����������� (�. isFull() ���).
		std::string console() - �����頥� ⥪�� ���ᠭ�� ��樨.
		clear() - ���뢠�� ��㬥���.
		�㭪�� - �����⪠�� �믮���⥫�, �᫨ � �㭪樨 ���� ��ࠬ��஢.
			� ��㣮� ��砥 ����뢠���� �᪫�祭�� OptionError("No single runnable option"). // to add
	Options::Parser(Options& opt) - �����.
		opt - ����� ��権, �� ����� �㤥� ��室��� ���ᨭ�.
		.parse(std::vector<std::string>) - ����� �����.
		bool .isError() - �ந��諠 �� �訡�� �� ���ᨭ��.
		.getErrors() - ���� �� �ந��襤訥 �訡�� � ���� std::vector<Options::Parser::Error*> (�. ����).
		.getOptions() - ���� १���⨢�� ��樨 � ��᢮���묨 ��㬥�⠬�.
	Options::Option::OptionData(<data>) - ���⥩��� ������.
		<data> - �� �� �����ন������ ⨯�� (�. ����)
		::Type - enum �����ন������ ⨯�� ������.
			UNKNOWN - �� ᮤ�ন� �����.
			STRING - std::string
			INT - int
			BOOL - bool
		�����ন������ ���� ���⮢���� � �����ন����� ⨯�.
			�� ������������ ����� ���⮢���� ��ᠥ��� �᪫�祭��
				OptionError("Wrong cast at Options::Option::OptionData: <description>"),
			��� <description> ���� ���஡��� ���ᠭ�� �ந��襤襣�.
		::Type .typeData - �����頥� ⨯ ������.
		std::string cast(::Type) - ��⠥��� �८�ࠧ����� ����� � 㪠����� ⨯.
			�� �ᯥ� �����頥� ������ ��ப�. �� �訡�� - �� ���ᠭ�� (�. Options::Parser::Error).
	Optinos::Parser::Error(<data>) - �訡�� �����.
		<data>:
			�) Options::Option& - ����, � ���ன �ந��諠 �訡�� (����易⥫쭮). // to add
			�) std::string - ���ᠭ�� �訡��.
				�������� �訡��:
					"No arguments" - ��� ��樨 �� 墠⨫� ��㬥�⮢ �� ���ᨭ��. ���� �ਫ�������.
					"Unknown node '<flag>'" - ����� ����⨫ ��������� 䫠�.
					"<error_description>" - �訡�� � �������� (�. ���).
		.console() - �����頥� ���ᠭ�� �訡�� � ���ᠭ��� �㭪樨, �᫨ ⠪���� �������.
	Options::Parser::Helper()
		Helper::argCVparse(int, char**) - �८�ࠧ�� ����� �� CMD � Helper::VecStr
		Helper::VecStr - std::vector<Helper::String> � �����প�� .join(std::string) (�. JavaScript: join). :)
			�����ন������ ���� ���⮢���� � std::vector<std::string>.
		Helper::String - std::string � �����প�� .split(std::string) (�. JavaScript: split).
			�����ন������ ���� ���⮢���� � std::string.
-----------------------------------------------------------------------------------------------------------------------
	2. ������ ����.
-----------------------------------------------------------------------------------------------------------------------
	
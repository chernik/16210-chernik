#include "glifedriver.h"

int main(int argc, char** argv){
	using namespace Chernik;
	GLifeDriver drv;
	drv.argCV(argc, argv);
	try{
		drv.go();
	} catch(Options::OptionError& err){
		std::cout << "Option error > " << err.what() << "\n";
	}
}
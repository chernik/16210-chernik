#ifndef GLIFEDRIVER_H
#define GLIFEDRIVER_H

// ���� ���� - ������ �� ���
#define newOptions std::make_shared<Options>

/*
== [[glifedriver.h]]
�ࠩ�� ��� �ࠢ����� <<GLife>> �१ <<Options>>
*/

#include <fstream>

#include "glife.h"
#include "glifegui.h"

namespace Chernik {
	
	class GLifeDriver {
	public:
		GLifeDriver();
		
		GLifeDriver(const GLifeDriver&);
		GLifeDriver& operator=(const GLifeDriver&);
		
		/*
		== [[GLifeDriver::argCV()]]
		�������� � ����� ��㬥��� ���������� ��ப� ��� �� ��᫥���饩 ��ࠡ�⪨
		*/
		void argCV(int, char**);
		
		/*
		== [[GLifeDriver::go()]]
		����᪠�� �ࠩ���
		*/
		void go();
		
		virtual ~GLifeDriver(){};
	private:
		typedef std::shared_ptr<Options> pOptions; // + newOptions
		
		Options::Parser::Helper::VecStr argv; // ��㬥��� cmd
		GLife obj; // ���
		
		/*
		== [[GLifeDriver::addHelp()]]
		addHelp(pOptions, name, flags, description)
		�������� � pOptions ����, �뢮����� ���ᠭ�� ��⠫�� ��樨 �� pOption
		*/
		void addHelp(pOptions, std::string, const Options::Option::VecStr&, std::string);
		
		std::string saveFile(std::string); // ���࠭��� � 䠩� � ������
		std::string loadFile(std::string); // ����㧨�� �� 䠩�� � ������
		
		void goArgCV(); // ����� ��ࠡ�⪨ ��㬥�⮢ cmd
		void goConsole(); // ����� ���ࠪ⨢���� ०���
		void goGUI(); // ����� GUI
		
		pOptions getOptArgCV(); // Options ��� ��㬥�⮢ cmd
		pOptions getOptConsole(); // Options ��� ���ࠪ⨢�
		pOptions getOptGUI(); // Options ��� GUI
		pOptions getOptFile(); // Options ��� 䠩�� ���ﭨ�
	};
	
}

#endif
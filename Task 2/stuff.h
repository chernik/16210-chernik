#ifndef STUFF_H
#define STUFF_H

#include <vector>
#include <memory>

namespace Chernik {

	template <class T>
	class Matrix {
	public:
		Matrix();
		
		Matrix(const Matrix&);
		Matrix& operator=(const Matrix&);
		
		T& get(int, int);
		void set(int, int, T);
		void resize(int, int);
		int getSizeX();
		int getSizeY();
		void clear();
		
		virtual ~Matrix();
	private:
		std::vector<T> arr;
		int Xsize, Ysize;
	};
// --------------------------------------
	template <class T>
	class Data {
	public:
		Data();
		
		Data(const Data&);
		Data& operator=(const Data&);
		
		T get();
		void set(T);
		void setDef(T);
		bool isDef();
		
		virtual ~Data();
	private:
		T data;
		T dataDef;
	};
	
	
/* �᫨ �뭮��� ���쭥�訩 ��᮪ � �⤥��� 䠩�, � �訡�� ��������. �����ॢ��, � ᯨ᪥ ��ࠬ��஢ �� ���� 㪠�뢠�� �����, �� ��� ���� ���� ��।��뢠�� �ਯ�. */
// ------------------------------------------------------------------------------------- MATRIX
	template <class T>
	Matrix<T>::Matrix(): Xsize(0), Ysize(0) {};
// ----------
	template <class T>
	Matrix<T>::Matrix(const Matrix<T>& other){
		this->arr = other.arr;
		this->Xsize = other.Xsize;
		this->Ysize = other.Ysize;
	}
	
	template <class T>
	Matrix<T>& Matrix<T>::operator=(const Matrix<T>& other){
		this->arr = other.arr;
		this->Xsize = other.Xsize;
		this->Ysize = other.Ysize;
		return *this;
	}
// ---------
	template <class T>
	T& Matrix<T>::get(int x, int y){
		if(x < 0 || x >= this->Xsize)
			throw std::runtime_error("Wrong X size");
		if(y < 0 || y >= this->Ysize)
			throw std::runtime_error("Wrong Y size");
		return this->arr[x*this->Ysize + y];
	}
	
	template <class T>
	void Matrix<T>::set(int x, int y, T data){
		if(x < 0 || x >= this->Xsize)
			throw std::runtime_error("Wrong X size");
		if(y < 0 || y >= this->Ysize)
			throw std::runtime_error("Wrong Y size");
		this->arr[x*this->Ysize + y] = data;
	}
	
	template <class T>
	void Matrix<T>::resize(int x, int y){
		std::vector<T> arr(x*y);
		int i, j;
		for(i = 0; i < x; i++)
		for(j = 0; j < y; j++)
			if(i < this->Xsize && j < this->Ysize)
				arr[i*y + j] = this->arr[i*this->Ysize + j];
		this->arr = arr;
		this->Xsize = x;
		this->Ysize = y;
	}
	
	template <class T>
	int Matrix<T>::getSizeX(){
		return this->Xsize;
	}
	
	template <class T>
	int Matrix<T>::getSizeY(){
		return this->Ysize;
	}
	
	template <class T>
	void Matrix<T>::clear(){
		auto x = this->Xsize;
		auto y = this->Ysize;
		this->resize(0, 0);
		this->resize(x, y);
	}
// ---------
	template <class T>
	Matrix<T>::~Matrix(){};
// ---------------------------------------------------------------------------------------- DATA
	template <class T>
	Data<T>::Data(){};
// ----------
	template <class T>
	Data<T>::Data(const Data<T>& other){
		this->data = other.data;
		this->dataDef = other.dataDef;
	}
	
	template <class T>
	Data<T>& Data<T>::
	operator=(const Data<T>& other){
		this->data = other.data;
		this->dataDef = other.dataDef;
		return *this;
	}
// ----------
	template <class T>
	T Data<T>::get(){
		return this->data;
	}
	
	template <class T>
	void Data<T>::set(T data){
		this->data = data;
	}
	
	template <class T>
	void Data<T>::setDef(T data){
		this->data = data;
		this->dataDef = data;
	}
	
	template <class T>
	bool Data<T>::isDef(){
		return this->data == this->dataDef;
	}
// ---------
	template <class T> Data<T>::~Data(){};
}

#endif
#ifndef INITGL_H
#define INITGL_H

/*
== [[initgl.h]]
OpenGL �ࠩ���
��⮬ ������ ���ᠭ��
*/

#include <GL/freeglut.h>
#include <string>
#include <stdexcept>
#include <functional>

#include <iostream>

namespace Chernik {

	class GLdriver {
	public:
		class floatArr;
		
		GLdriver();
		
		void setArg(int, char**);
		void setWinSize(int, int);
		void setWinPos(int, int);
		void setWinCapt(std::string);
		
		void setEventDraw(void(*)());
		void setEventMoveM(void(*)(int, int));
		void setEventButM(void(*)(int,int,int,int));
		void setEventKey(void(*)(unsigned char, int,int));
		void setEventVisible(void(*)(int));
		
		floatArr setCamPos();
		floatArr setCamLook();
		floatArr setCamUp();
		
		void operator()();
		
		void startDraw();
		void endDraw();
		
	private:
		void setDefault();
		void init();
		void reCamera();
		
		std::string appName;
		int winSize[2];
		int winPos[2];
		std::string winCapt;
		void(*eventDraw)();
		void(*eventMoveM)(int, int);
		void(*eventButM)(int,int,int,int);
		void(*eventKey)(unsigned char, int,int);
		void(*eventVisible)(int);
		float BGcolor[4];
		float camera[3][3];
	};

	class GLdriver::floatArr {
	public:
		typedef std::function<float(int, float, int)> lmbd;
		
		floatArr(lmbd);
		floatArr(const floatArr&);
		
		floatArr operator()(float);
		floatArr operator--(int);
		float operator++(int);
	private:
		int index;
		lmbd f;
	};
	
};

#endif
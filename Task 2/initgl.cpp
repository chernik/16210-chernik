#include "initgl.h"

namespace Chernik {
// -------------------------------------------
	GLdriver::GLdriver(){
		this->setDefault();
	}
// ------------------------------------------------
	void GLdriver::setArg(int argc, char** argv){
		if(argc >= 1)
			this->appName = argv[0];
	}
	
	void GLdriver::setWinSize(int w, int h){
		this->winSize[0] = w;
		this->winSize[1] = h;
	}
	
	void GLdriver::setWinPos(int x, int y){
		this->winPos[0] = x;
		this->winPos[1] = y;
	}
	
	void GLdriver::setWinCapt(std::string capt){
		this->winCapt = capt;
	}
// --------------------------------------------------------
	void GLdriver::setEventDraw(void(*f)()){
		this->eventDraw = f;
	}
	
	void GLdriver::setEventMoveM(void(*f)(int, int)){
		this->eventMoveM = f;
	}
	
	void GLdriver::setEventButM(void(*f)(int,int,int,int)){
		this->eventButM = f;
	}
	
	void GLdriver::setEventKey(void(*f)(unsigned char, int,int)){
		this->eventKey = f;
	}
	
	void GLdriver::setEventVisible(void(*f)(int)){
		this->eventVisible = f;
	}
// -----------------------------------------------------------
	GLdriver::floatArr GLdriver::setCamPos(){
		return floatArr([this](int index, float val, int wdo){
			if(wdo == 0)
				this->camera[0][index] = val;
			return this->camera[0][index];
		});
	}
// ----------------------------------------------------------
	void GLdriver::operator()(){
		this->init();
	}
// -----------------------------------------------------
	void GLdriver::startDraw(){
		glClearColor(
			this->BGcolor[0],
			this->BGcolor[1],
			this->BGcolor[2],
			this->BGcolor[3]);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		this->reCamera();
	}
	
	void GLdriver::endDraw(){
		glutSwapBuffers();
	}
// ---------------------------------------------------
	void GLdriver::setDefault(){
		this->appName = "<unknown>";
		this->winSize[0] = 300;
		this->winSize[1] = 300;
		this->winPos[0] = 0;
		this->winPos[1] = 0;
		this->winCapt = "OpenGL";
		this->eventDraw = 0;
		this->eventMoveM = 0;
		this->eventButM = 0;
		this->eventKey = 0;
		this->eventVisible = 0;
		this->BGcolor[0] = 1.0;
		this->BGcolor[1] = 1.0;
		this->BGcolor[2] = 1.0;
		this->BGcolor[3] = 1.0;
		this->camera[0][0] = 1.0;
		this->camera[0][1] = 1.0;
		this->camera[0][2] = 5.0;
		this->camera[1][0] = 0.0;
		this->camera[1][1] = 0.0;
		this->camera[1][2] = 0.0;
		this->camera[2][0] = 0.0;
		this->camera[2][1] = 1.0;
		this->camera[2][2] = 0.0;
	}
	
	void GLdriver::init(){
		if(this->eventDraw == 0)
			throw std::runtime_error("Chernik::GLdriver::init(): no 'void eventDraw()'");
		[this](){
			int argc = 1;
			char* argv[1] = {0};
			argv[0] = new char[this->appName.size() + 1];
			unsigned int i;
			for(i = 0; i < this->appName.size(); i++)
				argv[0][i] = this->appName[i];
			argv[0][this->appName.size()] = 0;
			glutInit(&argc, argv);
		}();
		glutInitWindowSize(this->winSize[0], this->winSize[1]);
		glutInitWindowPosition(this->winPos[0], this->winPos[1]);
		glutCreateWindow(this->winCapt.c_str());
		if(this->eventDraw != 0)
			glutDisplayFunc(this->eventDraw);
		if(this->eventMoveM != 0){
			glutMotionFunc(this->eventMoveM);
			glutPassiveMotionFunc(this->eventMoveM);
		}
		if(this->eventButM != 0)
			glutMouseFunc(this->eventButM);
		if(this->eventKey != 0)
			glutKeyboardFunc(this->eventKey);
		if(this->eventVisible != 0)
			glutVisibilityFunc(this->eventVisible);
		glEnable(GL_DEPTH_TEST);
		glMatrixMode(GL_PROJECTION);
		gluPerspective(40.0, 1.0, 1.0, 100.0);
		glMatrixMode(GL_MODELVIEW);
		this->reCamera();
		glutInitDisplayMode(GLUT_DOUBLE);
		glutMainLoop();
	}
	
	void GLdriver::reCamera(){
		glLoadIdentity();
		gluLookAt(
			this->camera[0][0], // center of camera
			this->camera[0][1],
			this->camera[0][2],
			this->camera[1][0], // look at
			this->camera[1][1],
			this->camera[1][2],
			this->camera[2][0], // where is up
			this->camera[2][1],
			this->camera[2][2]);
	}
// -------------------------------------------------
	GLdriver::floatArr::floatArr(lmbd f){
		this->f = f;
		this->index = 0;
	}
	
	GLdriver::floatArr::floatArr(const floatArr& other){
		this->f = other.f;
		this->index = other.index;
	}
	
	GLdriver::floatArr GLdriver::floatArr::operator()(float val){
		this->f(this->index, val, 0);
		this->index++;
		return *this;
	}
	
	GLdriver::floatArr GLdriver::floatArr::operator--(int){
		this->index++;
		return *this;
	}
	
	float GLdriver::floatArr::operator++(int){
		return this->f(this->index, 0, 1);
	}
// ----------------------------------------------
};
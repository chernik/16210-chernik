#include "glife.h"

namespace Chernik {
// ----------------------------------------------------------------------------------------------- GLIFE
	GLife::GLife(){
		this->states.push_back(
			std::make_shared<GLifeState>
			(*this));
		this->isBack = true;
	}
// ---------
	GLife::GLife(const GLife& other){
		this->states = other.states;
		this->isBack = other.isBack;
	}
	
	GLife& GLife::operator=(const GLife& other){
		this->states = other.states;
		this->isBack = other.isBack;
		return *this;
	}
// ---------
	Matrix<int> GLife::getMatrix(){
		return this->states.back()->matrix;
	}
	
	GLife::MapData GLife::getData(){
		auto& state = *this->states.back();
		return {
			{"M", state.M},
			{"N", state.N},
			{"K", state.K},
			{"X", state.X},
			{"Y", state.Y}
		};
	}
	
	void GLife::setBack(bool fl){
		this->isBack = fl;
	}
// ---------
	#include "glifecom.hpp" // ������塞 ��᮪ � �������� �㭪樮����
// ---------
	GLife::~GLife(){};
// ---------
	void GLife::doubleState(){
		if(this->isBack)
			this->states.push_back(
				std::make_shared<GLifeState>(*this->states.back()));
	}
// ------------------------------------------------------------------------------------ GLIFE::GLIFESTATE
	GLife::GLifeState::GLifeState(GLife& life){
		this->M.setDef(3);
		this->N.setDef(2);
		this->K.setDef(3);
		this->X.setDef(10);
		this->Y.setDef(10);
		this->matrix.resize
			(this->X.get(), this->Y.get());
	}
// -----------
	GLife::GLifeState::
	GLifeState(const GLifeState& other){
		this->M = other.M;
		this->N = other.N;
		this->K = other.K;
		this->X = other.X;
		this->Y = other.Y;
		this->matrix = other.matrix;
	}
	
	GLife::GLifeState&
	GLife::GLifeState::
	operator=(const GLifeState& other){
		this->M = other.M;
		this->N = other.N;
		this->K = other.K;
		this->X = other.X;
		this->Y = other.Y;
		this->matrix = other.matrix;
		return *this;
	}
	
	GLife::GLifeState::~GLifeState(){};
// ------------------------------------ ALL
}
	
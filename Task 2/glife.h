#ifndef GLIFE_H
#define GLIFE_H

/*
== glife.h
����ন� ������ ���� "�����"
*/

#include <vector>
#include <memory>
#include <iostream>

#include "opt.h"
#include "stuff.h"

namespace Chernik {
	
	/*
	== [[GLife]]
	������ ���� "�����"
	�।�⠢��� ᮡ�� ��אַ㣮�쭮� �ந���쭮� ���� ࠧ��� X �� Y (�� 㬮�砭�� 10 �� 10),
	     � ���ன ���⠢���� ���� ���⪨
	��� ���। �������� �� �ࠢ���� M, N, K (�� 㬮�砭�� 3, 2, 3 ᮮ⢥��⢥���), �. readme.md
	���� ���� ��࠭�����
	*/
	class GLife {
	public:
		/*
		== [[GLife::MapData]]
		������ std::string <=> <<Data>> ⨯� <int>
		*/
		typedef std::unordered_map<std::string, Data<int>, std::hash<std::string>> MapData;
		
		class GLifeState;
		
		GLife();
		
		/*
		== GLife moveing
		��樨, �����饭�� ����� ��ꥪ⮬ <<GLife>>, �� ������ �� ��� ����� (�. <<GLife::getRules()>>)
		*/
		GLife(const GLife&);
		GLife& operator=(const GLife&);
		
		/*
		== [[GLife::getRules()]]
		��६ �������� <<Options>> ��� �ࠢ����� <<GLife>>
		.�����ন������ ᫥���騥 ��樨:
		* *nil*                - ����ࠫ쭠� ����, �� ����� �� �� ��
		                         ����� �ᯮ�짮������ ��� ��㬥�� � �������� �����
		* *reset*              - ��頥� ����
		* *set <x> <y>*        - ������� ����� �� ���न��⠬
		* *clear <x> <y>*      - ��頥� ����� �� ���न��⠬
		* *stepone*            - ������ ���� 蠣 ���।
		                         ������� �������㥬��, �. <<GLife::setBack()>>
		* *step <n>*           - ������ N 蠣�� ���।
		                         �����ন������ n = *nil* � ����⢥ n = 1
		* *back*               - ������ ���� 蠣 �����
		* *rules <m> <n> <k>*  - ��⠭�������� ���� �ࠢ��� M, N, K
		                         �����ন������ m,n,k = *nil* � ����⢥ �ࠢ���, ���஥ �� �㦭� ��������
		                         ���ᠭ�� ��ࠬ��஢ M, N, K ᮤ�ঠ��� � *readme.md*
		* *field <x> <y>*      - ��⠭�������� ࠧ��� ��࠭�
		� ��� �����㥬�� ��権 䫠� ࠢ�� �����, �� ����� ���� ������� (�. <<Options>>)
		��᫥ 㤠����� ��ꥪ� <<GLife>> �����饭�� ��樨 ����� *undefined behaviour*
		*/
		Options getRules();
		
		/*
		== [[GLife::getData()]]
		��६ ����� M, N, K, X, Y ��� <<GLife::MapData>>
		*/
		MapData getData();
		
		/*
		== [[GLife::setBack()]]
		��⠭��������, �㦭� �� ��࠭��� �����
		*/
		void setBack(bool);
		
		/*
		== [[GLife::doubleState()]]
		�������� ⥪�饥 ���ﭨ� � �����
		*/
		void doubleState();
		
		/*
		== [[GLife::getMatrix()]]
		���� ������ <<Matrix>> ⨯� <int>, ������� ���ﭨ�� ����
		1 - ���� �����
		0 - ���� �����
		*/
		Matrix<int> getMatrix();
		
		virtual ~GLife();
	private:
		typedef std::vector<std::shared_ptr<GLifeState>> VecState;
		
		VecState states;
		bool isBack;
	};
// -------------------------------
	class GLife::GLifeState {
	public:
		/*
		== GLifeState()
		��������� �ॡ�� த�⥫�
		*/
		GLifeState(GLife&);
		
		/*
		== GLifeState moveing
		*/
		GLifeState(const GLifeState&);
		GLifeState& operator=(const GLifeState&);
		
		virtual ~GLifeState();
	private:
		friend class GLife; // ��ࠢ����� ���� �� ����� GLife
		Matrix<int> matrix; // ����ন� ������ ���ﭨ� ����...
		Data<int> M, N, K, X, Y; // ...� ��ࠬ���� ����
	};
// -----------------------------------------
}

#endif
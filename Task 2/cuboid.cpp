#include "cuboid.h"

namespace Chernik {
	// ---------------------- MATRIXCUBE
	MatrixCube::MatrixCube(){
		sizeX = 0.2;
		sizeY = 0.2;
		hL = 0.1;
		hH = 0.5;
		color = {1, 0, 0, 0, 1, 0, 0, 0, 1};
	}
	// ---------
	MatrixCube::MatrixCube(const MatrixCube& other){
		this->sizeX	= other.sizeX;
		this->sizeY	= other.sizeY;
		this->hL	= other.hL;
		this->hH	= other.hH;
		this->color	= other.color;
	}
	
	MatrixCube& MatrixCube::operator=(const MatrixCube& other){
		this->sizeX	= other.sizeX;
		this->sizeY	= other.sizeY;
		this->hL	= other.hL;
		this->hH	= other.hH;
		this->color	= other.color;
		return *this;
	}
	// --------
	void MatrixCube::out(){
		int i, j;
		for(i = 0; i < this->matrix.getSizeX(); i++)
		for(j = 0; j < this->matrix.getSizeY(); j++)
			this->matrix.get(i, j).out(*this, i, j);
	}
	
	MatrixCube& MatrixCube::operator<<(Matrix<int>& matrix){
		int i, j;
		this->matrix.resize(
			matrix.getSizeX(), matrix.getSizeY());
		for(i = 0; i < this->matrix.getSizeX(); i++)
		for(j = 0; j < this->matrix.getSizeY(); j++)
			this->matrix.get(i, j).
				set(matrix.get(i, j));
		return *this;
	}
	// ----------
	MatrixCube::~MatrixCube(){};
	// ---------------- MATRIXCUBE::CUBE
	MatrixCube::Cube::Cube():res(false){};
	// -----------
	MatrixCube::Cube::Cube(const Cube& other){
		this->res = other.res;
	}
	
	MatrixCube::Cube& MatrixCube::Cube::operator=(const MatrixCube::Cube& other){
		this->res = other.res;
		return *this;
	}
	// ------------
	void MatrixCube::Cube::out(MatrixCube& parent, int x, int y){
		float posX = -parent.matrix.getSizeX() * parent.sizeX / 2;
		float posY = -parent.matrix.getSizeY() * parent.sizeY / 2;
		glBegin(GL_QUADS);
		glColor3f(
			parent.color[6],
			parent.color[7],
			parent.color[8]);
		glVertex3f(
			posX + (0+x) * parent.sizeX,
			posY + (0+y) * parent.sizeY,
			0);
		glVertex3f(
			posX + (1+x) * parent.sizeX,
			posY + (0+y) * parent.sizeY,
			0);
		glVertex3f(
			posX + (1+x) * parent.sizeX,
			posY + (1+y) * parent.sizeY,
			0);
		glVertex3f(
			posX + (0+x) * parent.sizeX,
			posY + (1+y) * parent.sizeY,
			0);
		glEnd();
		glBegin(GL_QUADS);
		if(this->res)
			glColor3f(
				parent.color[0],
				parent.color[1],
				parent.color[2]);
		else
			glColor3f(
				parent.color[3],
				parent.color[4],
				parent.color[5]);
		glVertex3f(
			posX + (0+x) * parent.sizeX,
			posY + (0+y) * parent.sizeY,
			this->res ? parent.hH : parent.hL);
		glVertex3f(
			posX + (1+x) * parent.sizeX,
			posY + (0+y) * parent.sizeY,
			this->res ? parent.hH : parent.hL);
		glVertex3f(
			posX + (1+x) * parent.sizeX,
			posY + (1+y) * parent.sizeY,
			this->res ? parent.hH : parent.hL);
		glVertex3f(
			posX + (0+x) * parent.sizeX,
			posY + (1+y) * parent.sizeY,
			this->res ? parent.hH : parent.hL);
		glEnd();
	}
	
	void MatrixCube::Cube::set(int res){
		this->res = res;
	}
	// ----------
	MatrixCube::Cube::~Cube(){};
	// ----------------- ALL
}
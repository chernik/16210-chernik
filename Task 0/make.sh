#/bin/bash 
g++ -std=c++11 -Wall -O2 -c Module1.h 
g++ -std=c++11 -Wall -O2 -c Module2.h 
g++ -std=c++11 -Wall -O2 -c Module3.h 
g++ -std=c++11 -Wall -O2 -c main.cpp 
g++ -std=c++11 -Wall -O2 -c Module1.cpp 
g++ -std=c++11 -Wall -O2 -c Module2.cpp 
g++ -std=c++11 -Wall -O2 -c Module3.cpp 
g++ -std=c++11 -Wall -O2 *.o -o "main.out" 

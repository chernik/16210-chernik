#include "module1.h"
#include "module2.h"
#include "module3.h"
#include <iostream>

using namespace std; // �⮡� �� ����� std::

int main(int argc, char** argv)
{	
	cout << Module1::getMyName() << "\n";
	cout << Module2::getMyName() << "\n";

	using namespace Module1;
	cout << getMyName() << "\n"; // (A) // १���� �㭪樨 �� ����� Module1
	cout << Module2::getMyName() << "\n";

	//using namespace Module2; // (B)
	//cout << getMyName() << "\n"; // COMPILATION ERROR (C) // ��ॣ�㦥���� �㭪�� �� ���� ����

	using Module2::getMyName; // �����塞
	cout << getMyName() << "\n"; // (D) // १���� �㭪樨 �� ����� Module2
	
	cout << Module3::getMyName() << "\n"; // �뢮��� Peter
}

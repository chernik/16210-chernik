#include "file_sort.h"

using std::string;
using std::ifstream;
using std::ofstream;
using std::list;

namespace Chernik {
	
	FileSort::FileSort(){
	}
	
	bool FileSort::isSucs(int err){
		return err == ERR_NO;
	}
	
	int FileSort::sort(string inp, string outp){
		ifstream inf;
		ofstream outf;
		inf.open(inp.c_str());
		if(!inf.good())
			return ERR_IN;
		outf.open(outp.c_str());
		if(!outf.good())
			return ERR_OUT;
		string str;
		list<string> lst;
		while(!inf.eof()){
			getline(inf, str);
			lst.push_back(str);
		}
		lst.sort();
		for(list<string>::iterator i = lst.begin(); i != lst.end(); ++i){
			outf << *i << "\n";
		}
		inf.close();
		outf.close();
		return ERR_NO;
	}
	
	string FileSort::getErr(int err){
		switch(err){
			case ERR_NO:
				return "Sucsess";
				break;
			case ERR_IN:
				return "Error whis input file";
				break;
			case ERR_OUT:
				return "Error whis output file";
				break;
			default:
				return "Undefined error";
		}
	}
};
#ifndef FILE_SORT_H
#define FILE_SORT_H

#include <string>
#include <fstream>
#include <list>

/*

FileSort
	Class who can sorts strings in file
FilSort::isSucs
	Return true if error code is sucsessful
FileSort::sort(string FileIn, string FileOut)
	This function gets strings from FileIn, sors them and push it in FileOut
FileSort::getErr(int errCode)
	Explaine error code in returning string

*/

namespace Chernik {
	class FileSort {
		private:
			// Error codes
			static const int ERR_NO = 0;
			static const int ERR_IN = 1;
			static const int ERR_OUT = 2;
		public:
			FileSort();
			bool isSucs(int);
			int sort(std::string, std::string);
			std::string getErr(int);
	};
};

#endif /* FILE_SORT_H */
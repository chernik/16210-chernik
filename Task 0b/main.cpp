#include "file_sort.h"
#include <iostream>
#include <cstdlib>

using std::cin;
using std::cout;

using Chernik::FileSort;

int main(int argc, char **argv){
	if(argc < 3){
		cout << "Usage: " << argv[0] << " <input> <output>\n";
		return EXIT_FAILURE;
	}
	FileSort fs;
	int err = fs.sort(argv[1], argv[2]);
	if(!fs.isSucs(err)){
		cout << "Error: " << fs.getErr(err) << "\n";
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
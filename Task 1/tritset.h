#ifndef TRITSET_H
#define TRITSET_H

#include <vector>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <unordered_map>

#include <iostream>

/*
TritSet::Trit - container for save trits
TritSet::uint - unsigned int
TritSet::TritMap - unordered map <TritSet::Trit, int>
TritSet() - create set whis min size
TritSet(int) - create set whis user size
TritSet(TritSet) - create set from other set
TritSet= - recover from other set
TritSet[a] = b - push 'b' by index 'a'
TritSet[a] - get data by index 'a'
int capacity() - count of bytes
int length() - minimal length for keeping set trits
TritSet & TritSet - trit operation (whis | and !)
void shrink() - set minimal memory for keeping set trits
int cardinality(TritSet::Trit) - get count of trits whis user value
TritMap cardinality() - get all counts
trim(int) - clear all trits higher than index
begin(), end() - standart iterator
cbegin(), cend() - standart citerator
*/

namespace Chernik {
	
	class TritSet {
		public:
			// Classes
			class TritHolder; // Element of TritSet
			class TritIterator; // Iterator
			class TritCIterator; // cIterator
			// Types
			class TritHash; // hash for container
			enum class Trit { // container
				FALSE = -1,
				TRUE = 1,
				UNKNOWN = 0
			};
			typedef unsigned int uint;
			typedef std::unordered_map<Trit, int, TritHash> TritMap;
			
			// Constructor
			TritSet();
			TritSet(int);
			TritSet(const TritSet& other);
			
			// Operators
			TritSet& operator= (const TritSet&);
			TritHolder operator[] (int);
			Trit operator[] (int) const;
			TritSet operator& (TritSet);
			TritSet operator| (TritSet);
			TritSet operator! ();
			
			// Methods
			int capacity();
			void shrink();
			int cardinality(Trit);
			TritMap cardinality();
			void trim(int);
			int length();
			
			// Iterator
			TritIterator begin();
			TritIterator end();
			
			// cIterator
			TritCIterator cbegin();
			TritCIterator cend();
		private:
			// Relationships
			friend class TritHolder;
			
			// Constants
			static const int ZERO = 0;
			static const int SIZE_UINT = sizeof(uint);
			static const int B2B = 8; // Bits in beat
			static const int B2T = 2; // Bits in trit
			static const int T2UINT = SIZE_UINT * B2B / B2T;
			static const int POW2B2T = pow(2, B2T);
			static const int AND_OPERATOR = 1;
			static const int OR_OPERATOR = 2;
			static const int NOT_OPERATOR = 3;
			
			// Variables
			uint *arr; // For trit collection
			int size; // For size of collection
			int lastSetTrit;
			int tritsTrueFalse[2];
			
			// Methods
			void mathTrueFalse(Trit, Trit, int);
			TritSet logic(TritSet&, int);
			Trit logicTrit(Trit, Trit, int);
			int minSize(int); // uint elements for Trit elements
			void resize(int); // Change arr for new size (can remove elements)
			uint Trit2uint(Trit); // Trit => uint
			Trit uint2Trit(uint) const; // uint => Trit
			void get(int, Trit*) const; // Get value by index
			void set(int, Trit); // Set value by index
	};
	
	class TritSet::TritHolder {
		public:
			TritHolder(TritSet&, int); // Get collection and index
			TritHolder& operator= (TritSet::Trit); // Set trit
			TritHolder& operator= (TritHolder&); // Set trit from set
			operator TritSet::Trit() const; // Get trit
		private:
			TritSet& ts; // For collection
			int index; // For index
	};
	
	class TritSet::TritIterator {
		public:
			TritIterator(TritSet&, int); // Get collection and index
			TritSet::TritHolder operator*();
			TritIterator& operator++();
			TritIterator& operator++(int);
			bool operator==(TritIterator);
			bool operator!=(TritIterator);
		private:
			TritSet& ts; // For collection
			int index; // For index
	};
	
	class TritSet::TritCIterator {
		public:
			TritCIterator(TritSet&, int); // Get collection and index
			TritSet::Trit operator*() const;
			TritCIterator& operator++();
			TritCIterator& operator++(int);
			bool operator==(TritCIterator);
			bool operator!=(TritCIterator);
		private:
			TritSet& ts; // For collection
			int index; // For index
	};
	
	class TritSet::TritHash {
		public:
			TritHash();
			TritSet::uint operator()(const TritSet::Trit&) const;
	};
};

#endif /* TRITSET_H */
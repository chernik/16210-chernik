#include "tritset.h"

namespace Chernik {
// -------------------------------------------------------------------------------------------------- TritSet
// ---------------------------------------------------------------------------------- Constructors
	TritSet::TritSet(){
		this->tritsTrueFalse[0] = this->tritsTrueFalse[1] = 0;
		this->lastSetTrit = -1;
		this->size = ZERO;
		this->arr = new uint[ZERO]();
	};
	
	TritSet::TritSet(int size){
		this->tritsTrueFalse[0] = this->tritsTrueFalse[1] = 0;
		this->lastSetTrit = -1;
		this->size = ZERO;
		this->arr = new uint[ZERO]();
		if(size < ZERO) // Wrong size
			throw std::runtime_error("Out of range at TritSet(int)");
		this->resize(size); // Set
	};
	
	TritSet::TritSet(const TritSet& other){
		this->tritsTrueFalse[0] = other.tritsTrueFalse[0];
		this->tritsTrueFalse[1] = other.tritsTrueFalse[1];
		this->lastSetTrit = other.lastSetTrit;
		this->size = ZERO;
		this->arr = new uint[ZERO]();
		this->resize(other.size);
		int i;
		for(i = 0; i < minSize(this->size); i++)
			this->arr[i] = other.arr[i];
	};
// ----------------------------------------------------------------------------------- Operators
	TritSet& TritSet::operator= (const TritSet& other){
		if(this == &other) // if 'a = a'
			return *this;
		this->tritsTrueFalse[0] = other.tritsTrueFalse[0];
		this->tritsTrueFalse[1] = other.tritsTrueFalse[1];
		this->lastSetTrit = other.lastSetTrit;
		this->resize(other.size);
		int i;
		for(i = 0; i < minSize(this->size); i++)
			this->arr[i] = other.arr[i];
		return *this;
	};
	
	TritSet::TritHolder TritSet::operator[] (int index) {
		return TritHolder(*this, index); // Work whis TritElement
	};
	
	TritSet::Trit TritSet::operator[] (int index) const {
		Trit buf;
		this->get(index, &buf);
		return buf;
	};
	
	TritSet TritSet::operator& (TritSet other) {
		return this->logic(other, AND_OPERATOR);
	};
	
	TritSet TritSet::operator| (TritSet other) {
		return this->logic(other, OR_OPERATOR);
	};
	
	TritSet TritSet::operator! () {
		return this->logic(*this, NOT_OPERATOR);
	};		
// ---------------------------------------------------------------------------------- Methods
	int TritSet::capacity(){
		return minSize(this->size) * SIZE_UINT; // Size per bytes
	};
	
	void TritSet::shrink(){
		this->resize(this->lastSetTrit);
	};
	
		
	int TritSet::cardinality(Trit el){
		switch(el){
			case Trit::TRUE:
				return this->tritsTrueFalse[0];
			case Trit::FALSE:
				return this->tritsTrueFalse[1];
			default:
				return this->lastSetTrit + 1 - this->tritsTrueFalse[0] - this->tritsTrueFalse[1];
		}
	};
	
	TritSet::TritMap TritSet::cardinality(){
		TritMap map({
			{Trit::TRUE, this->cardinality(Trit::TRUE)},
			{Trit::FALSE, this->cardinality(Trit::FALSE)},
			{Trit::UNKNOWN, this->cardinality(Trit::UNKNOWN)}
		});
		return map;
	};
	
	int TritSet::length(){
		return this->lastSetTrit + 1;
	};
	
	void TritSet::trim(int index){
		int i;
		for(i = this->lastSetTrit; i >= index; i--){
			if(this->operator[](i) == Trit::TRUE)
				this->tritsTrueFalse[0]--;
			if(this->operator[](i) == Trit::FALSE)
				this->tritsTrueFalse[1]--;
		};
		this->resize(index);
		for(i = index - 1; i >= 0 && this->operator[](i) == Trit::UNKNOWN; i--);
		this->lastSetTrit = i;
	};
	
	TritSet::TritIterator TritSet::begin(){
		return TritIterator(*this, 0);
	};
	
	TritSet::TritIterator TritSet::end(){
		return TritIterator(*this, this->length());
	};
// --------------------------------------------------------------------------------- Private methods
	TritSet TritSet::logic(TritSet &t2, int typeLogic){
		TritSet &t1 = *this;
		int size_max = t1.size > t2.size ? t1.size : t2.size;
		TritSet tres(size_max);
		int i;
		for(i = 0; i < size_max; i++)
			tres[i] = logicTrit(t1[i], t2[i], typeLogic);
		return tres;
	};
	
	TritSet::Trit TritSet::logicTrit(Trit t1, Trit t2, int typeLogic){
		switch(typeLogic){
			case AND_OPERATOR:
				if(t1 == Trit::FALSE || t2 == Trit::FALSE)
					return Trit::FALSE;
				if(t1 == Trit::TRUE && t2 == Trit::TRUE)
					return Trit::TRUE;
				break;
			case OR_OPERATOR:
				if(t1 == Trit::TRUE || t2 == Trit::TRUE)
					return Trit::TRUE;
				if(t1 == Trit::FALSE && t2 == Trit::FALSE)
					return Trit::FALSE;
				break;
			case NOT_OPERATOR:
				if(t1 == Trit::TRUE)
					return Trit::FALSE;
				if(t1 == Trit::FALSE)
					return Trit::TRUE;
				break;
		}
		return Trit::UNKNOWN;
	};
				
	int TritSet::minSize(int size){
		size += T2UINT - 1;
		return size / T2UINT;
	};
	
	void TritSet::resize(int size){
		int size_old = minSize(this->size);
		int size_new = minSize(size);
		uint *buf = this->arr;
		this->arr = new uint[size_new]();
		int i;
		for(i = 0; i < size_new; i++){
			if(i < size_old)
				this->arr[i] = buf[i];
			else
				this->arr[i] = ZERO;
		}
		delete[] buf;
		this->size = size;
	};
	
	TritSet::Trit TritSet::uint2Trit(uint val) const { // uint => Trit
		switch(val){
			case 0: return Trit::UNKNOWN;
			case 1: return Trit::TRUE;
			case 2: return Trit::FALSE;
			default: return Trit::UNKNOWN;
		}
	};
	
	TritSet::uint TritSet::Trit2uint(Trit val){ // Trit => uint
		switch(val){
			case Trit::UNKNOWN: return 0;
			case Trit::TRUE: return 1;
			case Trit::FALSE: return 2;
			default: return 3;
		}
	};
	
	void TritSet::get(int index, Trit* el) const {
		*el = Trit::UNKNOWN;
		if(index < 0 || index >= this->size) // If wrong index
			return;
		uint val = this->arr[index / T2UINT];
		val >>= SIZE_UINT * B2B - B2T * (index % T2UINT); // 121'2'121 => 121'2'
		val = val % POW2B2T; // 121'2' => '2'
		*el = uint2Trit(val);
	};
	
	void TritSet::set(int index, Trit el){
		if(index < 0)
			throw std::runtime_error("Out of range at 'TritSet::set' (maybe at 'TritSet[]'?)");
		if(index >= this->size){
			if(el == Trit::UNKNOWN)
				return;
			this->resize(index + 1);
		}
		Trit old;
		this->get(index, &old);
		uint val = this->arr[index / T2UINT];
		val -= Trit2uint(old) << (SIZE_UINT * B2B - B2T * (index % T2UINT));
		val += Trit2uint(el)  << (SIZE_UINT * B2B - B2T * (index % T2UINT));
		this->arr[index / T2UINT] = val;
		this->mathTrueFalse(old, el, index);
	};
	
	void TritSet::mathTrueFalse(Trit one, Trit two, int index){
		switch(one){
			case Trit::FALSE:
				this->tritsTrueFalse[1]--;
				break;
			case Trit::TRUE:
				this->tritsTrueFalse[0]--;
				break;
			default:
				break;
		}
		switch(two){
			case Trit::FALSE:
				this->tritsTrueFalse[1]++;
				break;
			case Trit::TRUE:
				this->tritsTrueFalse[0]++;
				break;
			default:
				break;
		}
		if(one == Trit::UNKNOWN && two != Trit::UNKNOWN && index > this->lastSetTrit) // Set
			this->lastSetTrit = index;
		if(one != Trit::UNKNOWN && two == Trit::UNKNOWN && index == this->lastSetTrit){ // Unset
			int i;
			for(
				i = this->lastSetTrit;
				i >= 0 && this->operator[](i) == Trit::UNKNOWN;
				i--
			);
			this->lastSetTrit = i;
		}
	};
// ----------------------------------------------------------------------------------------------- TritElement
// --------------------------------------------------------------- Constructors
	TritSet::TritHolder::TritHolder(TritSet& ts, int index) : ts(ts), index(index) {}; // Duplicate names, but no errors, no warnings?
// ----------------------------------------------------------------- Operators
	TritSet::TritHolder& TritSet::TritHolder::operator= (TritSet::Trit value){
		this->ts.set(index, value); // private method
		return *this;
	};
	TritSet::TritHolder& TritSet::TritHolder::operator= (TritSet::TritHolder& value){
		this->ts.set(index, value); // private method
		return *this;
	};
	
	TritSet::TritHolder::operator TritSet::Trit() const {
		TritSet::Trit ret = TritSet::Trit::UNKNOWN;
		this->ts.get(index, &ret); // private method
		return ret;
	};
// --------------------------------------------------------------------------------------------------- TritIterator
// --------------------------------------------------------Constructors
	TritSet::TritIterator::TritIterator(TritSet& ts, int index) : ts(ts), index(index) {};
// ----------------------------------------------------------------- Operators
	TritSet::TritHolder TritSet::TritIterator::operator*(){
		return this->ts[this->index];
	};
	
	TritSet::TritIterator& TritSet::TritIterator::operator++(){
		this->index++;
		return *this;
	};
	
	TritSet::TritIterator& TritSet::TritIterator::operator++(int){
		this->index++;
		return *this;
	};
	
	bool TritSet::TritIterator::operator==(TritSet::TritIterator other){
		return this->index == other.index;
	};
	
	bool TritSet::TritIterator::operator!=(TritSet::TritIterator other){
		return this->index != other.index;	
	};
// --------------------------------------------------------------------------------------------------- Trit�Iterator
// --------------------------------------------------------Constructors
	TritSet::TritCIterator::TritCIterator(TritSet& ts, int index) : ts(ts), index(index) {};
// ----------------------------------------------------------------- Operators
	TritSet::Trit TritSet::TritCIterator::operator*() const{
		return this->ts[this->index];
	};
	
	TritSet::TritCIterator& TritSet::TritCIterator::operator++(){
		this->index++;
		return *this;
	};
	
	TritSet::TritCIterator& TritSet::TritCIterator::operator++(int){
		this->index++;
		return *this;
	};
	
	bool TritSet::TritCIterator::operator==(TritSet::TritCIterator other){
		return this->index == other.index;
	};
	
	bool TritSet::TritCIterator::operator!=(TritSet::TritCIterator other){
		return this->index != other.index;	
	};
// --------------------------------------------------------------------------------------------------------- TritHash
	TritSet::TritHash::TritHash(){};
	
	TritSet::uint TritSet::TritHash::operator()(const TritSet::Trit& el) const{
		switch(el){
			case TritSet::Trit::TRUE:
				return 1;
			case TritSet::Trit::FALSE:
				return 2;
			default:
				return 0;
		};
	};
};
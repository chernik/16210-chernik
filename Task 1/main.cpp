//#include <gtest\gtest.h> // doesn't work at g++; too many flags, too few sense
#ifdef RUN_ALL_TESTS
#include "test.h"
#else
#include "mytest.h"
#endif

int main(int argc, char** argv){
	#ifdef RUN_ALL_TESTS
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
	#else
	Chernik::runMyTests();
	#endif
}